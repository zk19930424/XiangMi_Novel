//
//  BookDetailViewController.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookDetailViewController : UIViewController

@property (nonatomic,strong) NSString *bookId;

@end

NS_ASSUME_NONNULL_END
