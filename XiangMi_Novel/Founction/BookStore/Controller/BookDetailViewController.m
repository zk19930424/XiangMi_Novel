//
//  BookDetailViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import "BookDetailViewController.h"
#import "BookDetailCollectionViewCell.h"
#import "CatalogueViewController.h"

@interface BookDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSDictionary *dataDict;
@property (nonatomic,assign) CGFloat stringHeight;
@end

@implementation BookDetailViewController

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, AdaptNavigationHeight, DeviceWidth, DeviceHeight-SafeAreaBottomHeight-AdaptNavigationHeight-50*AdaptRatio) collectionViewLayout:layout];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.view addSubview:self.collectionView];
        self.collectionView.backgroundColor = ClearColor;
        self.collectionView.bounces = YES;
        self.collectionView.showsVerticalScrollIndicator = NO;
        
        [self.collectionView registerClass:[BookDetailCollectionViewCell0 class] forCellWithReuseIdentifier:@"BookDetailCollectionViewCell0"];
        [self.collectionView registerClass:[BookDetailCollectionViewCell1 class] forCellWithReuseIdentifier:@"BookDetailCollectionViewCell1"];
        [self.collectionView registerClass:[BookDetailCollectionViewCell2 class] forCellWithReuseIdentifier:@"BookDetailCollectionViewCell2"];
        [self.collectionView registerClass:[BookDetailCollectionViewCell3 class] forCellWithReuseIdentifier:@"BookDetailCollectionViewCell3"];
        [self.collectionView registerClass:[BookDetailCollectionViewCell4 class] forCellWithReuseIdentifier:@"BookDetailCollectionViewCell4"];
    }
    return _collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseUI:@"书籍详情"];
    self.stringHeight = 0.0;
    
    [self bottomView];
    self.collectionView.backgroundColor = WhiteColor;
    
    [self requestBookDetail];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

//请求 书籍详情
- (void)requestBookDetail
{
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:[RequestUrl theBookDetail_get] loadStr:@"" param:@{@"token":[Dao findUserInfo].token,@"bk_id":self.bookId} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            self.dataDict = [NSDictionary dictionary];

            self.dataDict = dictionary[@"result"];
            [self.collectionView reloadData];
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.dataDict) {
        return 6;
    }else{
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 1) {
        return 1;
    }else if (section == 2) {
        return 1;
    }else if (section == 3) {
        return 1;
    }else if (section == 4) {
        return 1;
    }else {
        return 3;//array
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        BookDetailCollectionViewCell0 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookDetailCollectionViewCell0" forIndexPath:indexPath];
        NSString *imageUrl = [NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"cover"]];
        SDWebImageWith(cell.imageV, imageUrl);
        cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"name"]];
        cell.autherLabel.text = [NSString stringWithFormat:@"%@ 著",self.dataDict[@"book"][@"authorName"]];
        cell.typeLabel.text = [NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"categoryName"]];
        
        NSString *wordCount = [NSString stringWithFormat:@"%@万",self.dataDict[@"book"][@"wordCount"]];
        if (wordCount.length > 4) {
            wordCount = [Tools separateString:wordCount length:wordCount.length-4 location:0];
            cell.wordNumberLabel.text = [NSString stringWithFormat:@"%@万",wordCount];
        }else if (wordCount.length == 4){
            wordCount = [Tools separateString:wordCount length:wordCount.length-3 location:0];
            cell.wordNumberLabel.text = [NSString stringWithFormat:@"%@千",wordCount];
        }else{
            cell.wordNumberLabel.text = wordCount;
        }
        
        NSString *fullFlag = [NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"fullFlag"]];
        if ([fullFlag isEqualToString:@"1"]) {
            fullFlag = @"完结";
        }else if ([fullFlag isEqualToString:@"0"]){
            fullFlag = @"连载";
        }else{
            fullFlag = @"封笔";
        }
        cell.stateLabel.text = fullFlag;
        cell.clickLabel1.text = [NSString stringWithFormat:@"%@万",self.dataDict[@"book"][@"visitWeek"]];
        cell.clickLabel2.text = [NSString stringWithFormat:@"%@万",self.dataDict[@"book"][@"visitMouth"]];
        cell.clickLabel3.text = [NSString stringWithFormat:@"%@万",self.dataDict[@"book"][@"visitAll"]];
        return cell;
    }else if (indexPath.section == 1){
        BookDetailCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookDetailCollectionViewCell1" forIndexPath:indexPath];
        cell.backgroundColor = BGLightGrayColor;
        return cell;
    }else if (indexPath.section == 2){
        BookDetailCollectionViewCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookDetailCollectionViewCell2" forIndexPath:indexPath];
        cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"intro"]];
        return cell;
    }else if (indexPath.section == 3){
        BookDetailCollectionViewCell3 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookDetailCollectionViewCell3" forIndexPath:indexPath];
        NSString *fullFlag = [NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"fullFlag"]];
        if ([fullFlag isEqualToString:@"1"]) {
            cell.centerLabel.text = [NSString stringWithFormat:@"完结共%@章",[NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"chapterTotal"]]];
        }else{
            cell.centerLabel.text = [NSString stringWithFormat:@"更新至%@章",[NSString stringWithFormat:@"%@",self.dataDict[@"book"][@"chapterTotal"]]];
        }
        return cell;
    }else if (indexPath.section == 4){
        BookDetailCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookDetailCollectionViewCell1" forIndexPath:indexPath];
        cell.backgroundColor = BGLightGrayColor;
        cell.leftLabel.text = @"猜你喜欢";
        return cell;
    }else{
        NSDictionary *dict = self.dataDict[@"relateBook"][indexPath.item];
        BookDetailCollectionViewCell4 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookDetailCollectionViewCell4" forIndexPath:indexPath];
        cell.backgroundColor = BGLightGrayColor;
        cell.titleLabel.text = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",dict[@"name"]]];
        NSString *imageUrl = [NSString stringWithFormat:@"%@",dict[@"cover"]];
        SDWebImageWith(cell.imageV, imageUrl);
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3) {
        CatalogueViewController *vc = [[CatalogueViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGSizeMake(DeviceWidth, 230*AdaptRatio);
    }else if (indexPath.section == 1){
        return CGSizeMake(DeviceWidth, 45*AdaptRatio);
    }else if (indexPath.section == 2){
        return CGSizeMake(DeviceWidth, 70*AdaptRatio);
    }else if (indexPath.section == 3){
        return CGSizeMake(DeviceWidth, 40*AdaptRatio);
    }else if (indexPath.section == 4){
        return CGSizeMake(DeviceWidth, 45*AdaptRatio);
    }else{
        return CGSizeMake(DeviceWidth/3, 200*AdaptRatio);
    }
}

- (void)bottomView
{
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight-50*AdaptRatio, DeviceWidth, 50*AdaptRatio)];
    view1.backgroundColor = [UIColor whiteColor];
    view1.userInteractionEnabled = YES;
    [self.view addSubview:view1];
    
    UIButton *addCarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [view1 addSubview:addCarBtn];
    addCarBtn.backgroundColor = WhiteColor;
    addCarBtn.frame = CGRectMake(0,0,view1.size.width*1/2,50*AdaptRatio);
    //[searchBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    addCarBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [addCarBtn setTitle:@"加入书架" forState:UIControlStateNormal];
    [addCarBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
    [addCarBtn addTarget:self action:@selector(addBookcaseClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *buyNowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [view1 addSubview:buyNowBtn];
    buyNowBtn.backgroundColor = ThemeColor;
    buyNowBtn.frame = CGRectMake(view1.size.width*1/2,0,view1.size.width*1/2,50*AdaptRatio);
    //[buyNowBtn setBackgroundImage:[UIImage imageNamed:@"button_rectangle"] forState:UIControlStateNormal];
    buyNowBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [buyNowBtn setTitle:@"立即阅读" forState:UIControlStateNormal];
    [buyNowBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [buyNowBtn addTarget:self action:@selector(readNowClick) forControlEvents:UIControlEventTouchUpInside];
}

//加入书架
- (void)addBookcaseClick
{
    NSDictionary *dict = @{@"token":[Dao findUserInfo].token,@"bk_id":self.bookId};
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"post" url:[RequestUrl theBookaddToBookcase_post] loadStr:@"" param:dict Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            [Tools showMessage:@"加入成功"];
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}
//立即阅读
- (void)readNowClick
{
    
}


- (void)baseUI:(NSString *)titleStr
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = WhiteColor;
    self.title = titleStr;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self.view addSubview:backBtn];
    //backBtn.backgroundColor = RedColor;
    backBtn.frame = CGRectMake(0,0,25,25);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backBtnItem;
}

- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
