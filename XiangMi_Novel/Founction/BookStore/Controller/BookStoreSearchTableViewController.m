//
//  BookStoreSearchTableViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import "BookStoreSearchTableViewController.h"

@interface BookStoreSearchTableViewController ()<UITextFieldDelegate>
@property (nonatomic,strong) UITextField *searchTF;

@end

@implementation BookStoreSearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseUI:@"搜索"];
    
    [self topView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.searchTF becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    ShopGoodsListViewController *vc = [[ShopGoodsListViewController alloc] init];
//    vc.title = textField.text;
//    vc.cidString = @"";
//    vc.typeIndex = 1;
//    [self.navigationController pushViewController:vc animated:YES];
    return YES;
}

//搜索栏视图
- (void)topView
{
    UIView *baseView = [[UIView alloc] init];
    baseView.backgroundColor = WhiteColor;
    baseView.frame = CGRectMake(0, 0, DeviceWidth, [Tools getStatusBarHeight]+44);
    [self.view addSubview:baseView];
    
    UIView *searchView = [[UIView alloc] init];
    searchView.backgroundColor = UIColorFromRGB(0xf7f7f7);
    searchView.frame = CGRectMake(15*AdaptRatio, [Tools getStatusBarHeight]+7*AdaptRatio, baseView.size.width-75, 30*AdaptRatio);
    [baseView addSubview:searchView];
    searchView.layer.masksToBounds = YES;
    searchView.layer.cornerRadius = 3;
    
    UIImageView *searchLogoImageView = [[UIImageView alloc] init];
    [searchView addSubview:searchLogoImageView];
    searchLogoImageView.frame = CGRectMake(5*AdaptRatio, 7.5*AdaptRatio, 15*AdaptRatio, 15*AdaptRatio);
    //searchLogoImageView.backgroundColor = OrangeColor;
    searchLogoImageView.image = [UIImage imageNamed:@"shop_search"];
    
    self.searchTF = [[UITextField alloc] initWithFrame:CGRectMake(30*AdaptRatio, 0, searchView.size.width-30*AdaptRatio, 30*AdaptRatio)];
    self.searchTF.backgroundColor = ClearColor;
    self.searchTF.placeholder = @"输入书名或作者";
    self.searchTF.textColor = BlackColor;
    self.searchTF.font = [UIFont systemFontOfSize:14*AdaptRatio];
    [searchView addSubview:self.searchTF];
    self.searchTF.delegate = self;
    self.searchTF.borderStyle = UITextBorderStyleNone;
    self.searchTF.keyboardType = UIKeyboardTypeDefault;
    //    self.searchTF.layer.borderColor = LightGrayColor.CGColor;
    //    self.searchTF.layer.masksToBounds = YES;
    //    self.searchTF.layer.borderWidth = 1;
    //    self.searchTF.layer.cornerRadius = 5*AdaptRatio;
    
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [baseView addSubview:searchBtn];
    //searchBtn.backgroundColor = UIColorFromRGB(0xff5e44);
    searchBtn.frame = CGRectMake(baseView.size.width-45*AdaptRatio,[Tools getStatusBarHeight]+7.5*AdaptRatio,30*AdaptRatio,25*AdaptRatio);
    //[searchBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:14*AdaptRatio];
    [searchBtn setTitle:@"取消" forState:UIControlStateNormal];
    [searchBtn setTitleColor:UIColorFromRGB(0xff5e44) forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)cancelBtnClick
{
    [self backBtnClick];
    [self.searchTF resignFirstResponder];
}

- (void)baseUI:(NSString *)titleStr
{
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = WhiteColor;
    self.title = titleStr;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self.view addSubview:backBtn];
    //backBtn.backgroundColor = RedColor;
    backBtn.frame = CGRectMake(0,0,25,25);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backBtnItem;
    
//    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    //rightBtn.backgroundColor = [UIColor redColor];
//    rightBtn.frame = CGRectMake(0,0,40*AdaptRatio,25*AdaptRatio);
//    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14*AdaptRatio];
//    [rightBtn setTitle:@"管理" forState:UIControlStateNormal];
//    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [rightBtn addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
}
- (void)rightBtnClick:(UIButton *)sender
{
   
}

- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
