//
//  BookstoreViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 Botao. All rights reserved.
//

#import "BookstoreViewController.h"
#import "BookstoreCollectionViewCell.h"
#import "BookStoreSearchTableViewController.h"
#import "BookDetailViewController.h"
#import "SaxTypeViewController.h"
#import "RankViewController.h"
#import "FreeViewController.h"
@interface BookstoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SDCycleScrollViewDelegate>
@property (nonatomic,strong) NSUserDefaults *userDefault;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,assign) NSInteger selectIndex;//1精选,2女生,3男生

@property (nonatomic,strong) NSMutableArray *sellingArray;
@property (nonatomic,assign) NSInteger pageNumber;
@property (nonatomic,assign) BOOL isHaveMore;

@property (nonatomic,strong) NSArray *bannerArray;
@property (nonatomic,strong) NSMutableArray *bannerImageArray;

@property (nonatomic,strong) NSMutableArray *recommenndArray;//重磅推荐
@property (nonatomic,strong) NSMutableArray *weekHotReadArray;//一周热读

@end

@implementation BookstoreViewController

- (NSUserDefaults *)userDefault
{
    if (_userDefault == nil) {
        _userDefault = [NSUserDefaults standardUserDefaults];
    }
    return _userDefault;
}

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 50*AdaptRatio+[Tools getStatusBarHeight], DeviceWidth, DeviceHeight-49-SafeAreaBottomHeight-50*AdaptRatio-[Tools getStatusBarHeight]) collectionViewLayout:layout];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.view addSubview:self.collectionView];
        self.collectionView.backgroundColor = ClearColor;
        self.collectionView.bounces = YES;
        self.collectionView.showsVerticalScrollIndicator = NO;
        
        [self.collectionView registerClass:[BookstoreCollectionViewCell0 class] forCellWithReuseIdentifier:@"BookstoreCollectionViewCell0"];
        [self.collectionView registerClass:[BookstoreCollectionViewCell1 class] forCellWithReuseIdentifier:@"BookstoreCollectionViewCell1"];
        [self.collectionView registerClass:[BookstoreCollectionViewCell2 class] forCellWithReuseIdentifier:@"BookstoreCollectionViewCell2"];
        [self.collectionView registerClass:[BookstoreCollectionViewCell3 class] forCellWithReuseIdentifier:@"BookstoreCollectionViewCell3"];
        [self.collectionView registerClass:[BookstoreCollectionViewCell4 class] forCellWithReuseIdentifier:@"BookstoreCollectionViewCell4"];
        [self.collectionView registerClass:[BookstoreCollectionViewCell5 class] forCellWithReuseIdentifier:@"BookstoreCollectionViewCell5"];
        [self.collectionView registerClass:[BookstoreCollectionViewCell6 class] forCellWithReuseIdentifier:@"BookstoreCollectionViewCell6"];
    }
    return _collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = WhiteColor;
    
    if ([self.userDefault boolForKey:@"sureLogin"] == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"presentLoginVC" object:nil];
        return;
    }
    
    self.selectIndex = 1;
    
    [self topView];
    
    self.isHaveMore = YES;
    self.pageNumber = 1;
    self.sellingArray = [NSMutableArray array];
    self.collectionView.backgroundColor = WhiteColor;
    [self setRefreshHeaderFooter];
    
    
    
    //请求
    [self request];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

//请求
- (void)request
{
    [self requestBanner];
    [self requestRecommennd];
    [self requestWeekHotRead];
    [self requestHotSelling];
}
/*
 if (self.selectIndex == 1) {
 
 }else if (self.selectIndex == 2){
 
 }else{
 
 }
 */
//请求 轮播图
- (void)requestBanner
{
    self.bannerArray = [NSArray array];
    self.bannerImageArray = [NSMutableArray array];
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:[RequestUrl theBookStoreBanner_get] loadStr:@"" param:@{@"token":[Dao findUserInfo].token} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            NSArray *array = dictionary[@"result"];
            self.bannerArray = array;
            for (NSDictionary *dict in array) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@",dict[@"urlPic"]];
                [self.bannerImageArray addObject:imageUrl];
            }
            [self.collectionView reloadData];
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}
//请求 重磅推荐
- (void)requestRecommennd
{
    self.recommenndArray = [NSMutableArray array];
    //channel: 1男生,2女生,3精选
    NSString *channel;
    if (self.selectIndex == 1) {
        channel = @"3";
    }else if (self.selectIndex == 2){
        channel = @"1";
    }else{
        channel = @"2";
    }
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:[RequestUrl theBookStoreRecommennd_get] loadStr:@"" param:@{@"token":[Dao findUserInfo].token,@"number":@"3",@"channel":channel} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            self.recommenndArray = dictionary[@"result"];
            if (self.recommenndArray.count == 0) {
                return ;
            }
            [self.collectionView reloadData];
            if (self.selectIndex == 1) {
                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:2]];
            }else{
                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:3]];
            }
            
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}
//请求 一周热读
- (void)requestWeekHotRead
{
    self.weekHotReadArray = [NSMutableArray array];
    //channel: 1男生,2女生,3精选
    NSString *channel;
    if (self.selectIndex == 1) {
        channel = @"3";
    }else if (self.selectIndex == 2){
        channel = @"1";
    }else{
        channel = @"2";
    }
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:[RequestUrl theBookStoreWeekHotRead_get] loadStr:@"" param:@{@"token":[Dao findUserInfo].token,@"number":@"3",@"channel":channel} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            self.weekHotReadArray = dictionary[@"result"];
            if (self.weekHotReadArray.count == 0) {
                return ;
            }
            [self.collectionView reloadData];
            if (self.selectIndex == 1) {
                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:4]];
            }else{
                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:4]];
            }
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}
//请求 畅销小说
- (void)requestHotSelling
{
    //channel: 1男生,2女生,3精选
    NSString *channel;
    if (self.selectIndex == 1) {
        channel = @"3";
    }else if (self.selectIndex == 2){
        channel = @"1";
    }else{
        channel = @"2";
    }
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:[RequestUrl theBookStoreHotSelling_get] loadStr:@"" param:@{@"token":[Dao findUserInfo].token,@"pageSize":@"9",@"pageNum":[NSString stringWithFormat:@"%ld",self.pageNumber]} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            NSArray *array = dictionary[@"result"];
            if (array.count < 9) {
                self.isHaveMore = NO;
            }
            if (array.count == 0) {
                return ;
            }
            for (NSDictionary *dict in array) {
                [self.sellingArray addObject:dict];
            }
            [self.collectionView reloadData];
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:6]];
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}


- (void)setRefreshHeaderFooter
{
    MJRefreshBackNormalFooter *footer = [[MJRefreshBackNormalFooter alloc] init];
    [footer setRefreshingTarget:self refreshingAction:@selector(footerClick)];
    self.collectionView.mj_footer = footer;
}

- (void)footerClick
{
    [self.collectionView.mj_footer endRefreshing];
    if (self.isHaveMore == NO) {
        [Tools showMessage:@"没有更多数据"];
        return;
    }
    self.pageNumber++;
    
    [self requestHotSelling];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.selectIndex == 1) {
        return 7;
    }else{
        return 7;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.selectIndex == 1) {
        if (section == 0) {
            return 1;
        }else if (section == 1) {
            return 1;
        }else if (section == 2) {
            return self.recommenndArray.count;//array
        }else if (section == 3) {
            return 1;
        }else if (section == 4) {
            return self.self.weekHotReadArray.count;//array
        }else if (section == 5) {
            return 1;
        }else {
            return self.sellingArray.count;//array
        }
    }else{
        if (section == 0) {
            return 1;
        }else if (section == 1) {
            return 3;
        }else if (section == 2) {
            return 1;
        }else if (section == 3) {
            return self.recommenndArray.count;//array
        }else if (section == 4) {
            return self.self.weekHotReadArray.count;//array
        }else if (section == 5) {
            return 1;
        }else {
            return self.sellingArray.count;//array
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectIndex == 1) {
        if (indexPath.section == 0) {
            BookstoreCollectionViewCell0 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell0" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = ClearColor;
            SDCycleScrollView *scrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, cell.size.width-30*AdaptRatio, cell.size.height-20*AdaptRatio) shouldInfiniteLoop:YES imageNamesGroup:self.bannerImageArray];
            scrollView.backgroundColor = ClearColor;
            scrollView.autoScrollTimeInterval = 2;
            //[scrollView.layer setCornerRadius:10.0f];
            //scrollView.layer.masksToBounds = YES;
            scrollView.pageDotColor = ThemeColor;
            scrollView.delegate = self;
            [cell addSubview:scrollView];
            if (self.bannerImageArray.count<2) {
                scrollView.autoScroll = NO;
            }
            scrollView.layer.masksToBounds = YES;
            scrollView.layer.cornerRadius = 3;
            return cell;
        }else if (indexPath.section == 1) {
            BookstoreCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell1" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = ClearColor;
            cell.leftLabel.text = @"重磅推荐";
            return cell;
        }else if (indexPath.section == 2) {
            BookstoreCollectionViewCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell2" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = ClearColor;
            cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.recommenndArray[indexPath.item][@"name"]];
            cell.autherLabel.text = [NSString stringWithFormat:@"%@",self.recommenndArray[indexPath.item][@"authorName"]];
            [cell.autherLabel sizeToFit];
            NSString *imageUrl = [NSString stringWithFormat:@"%@",self.recommenndArray[indexPath.item][@"cover"]];
            SDWebImageWith(cell.imageV, imageUrl);
            return cell;
        }else if (indexPath.section == 3) {
            BookstoreCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell1" forIndexPath:indexPath];
            cell.backgroundColor = BGLightGrayColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = WhiteColor;
            cell.leftLabel.text = @"一周热读";
            return cell;
        }else if (indexPath.section == 4) {
            BookstoreCollectionViewCell3 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell3" forIndexPath:indexPath];
            cell.backgroundColor = BGLightGrayColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = WhiteColor;
            cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"name"]];
            cell.autherLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"authorName"]];
            [cell.autherLabel sizeToFit];
            NSString *imageUrl = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"cover"]];
            SDWebImageWith(cell.headerImageView, imageUrl);
            cell.detailLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"intro"]];
            cell.typeLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"categoryName"]];
            return cell;
        }else if (indexPath.section == 5) {
            BookstoreCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell1" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = WhiteColor;
            cell.leftLabel.text = @"畅销小说";
            return cell;
        }else {
            BookstoreCollectionViewCell4 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell4" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = ClearColor;
            cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.sellingArray[indexPath.item][@"name"]];
            NSString *imageUrl = [NSString stringWithFormat:@"%@",self.sellingArray[indexPath.item][@"cover"]];
            SDWebImageWith(cell.imageV, imageUrl);
            return cell;
        }
    }else if (self.selectIndex == 2){
        if (indexPath.section == 0) {
            BookstoreCollectionViewCell0 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell0" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = ClearColor;
            SDCycleScrollView *scrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, cell.size.width-30*AdaptRatio, cell.size.height-20*AdaptRatio) shouldInfiniteLoop:YES imageNamesGroup:self.bannerImageArray];
            scrollView.backgroundColor = ClearColor;
            scrollView.autoScrollTimeInterval = 2;
            //[scrollView.layer setCornerRadius:10.0f];
            //scrollView.layer.masksToBounds = YES;
            scrollView.pageDotColor = ThemeColor;
            scrollView.delegate = self;
            [cell addSubview:scrollView];
            if (self.bannerImageArray.count<2) {
                scrollView.autoScroll = NO;
            }
            scrollView.layer.masksToBounds = YES;
            scrollView.layer.cornerRadius = 3;
            return cell;
        }else if (indexPath.section == 1) {
            BookstoreCollectionViewCell5 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell5" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = ClearColor;
            if (indexPath.item == 0) {
                cell.titleLabel.text = @"分类";
                cell.imageV.image = [UIImage imageNamed:@"bookstore_type"];
            }else if (indexPath.item == 1){
                cell.titleLabel.text = @"排行";
                cell.imageV.image = [UIImage imageNamed:@"bookstore_charts"];
            }else{
                cell.titleLabel.text = @"免费";
                cell.imageV.image = [UIImage imageNamed:@"bookstore_free"];
            }
            return cell;
        }else if (indexPath.section == 2) {
            BookstoreCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell1" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = WhiteColor;
            cell.leftLabel.text = @"重磅推荐";
            return cell;
        }else if (indexPath.section == 3) {
            BookstoreCollectionViewCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell2" forIndexPath:indexPath];
            cell.backgroundColor = BGLightGrayColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = WhiteColor;
            
            return cell;
        }else if (indexPath.section == 4) {
            BookstoreCollectionViewCell6 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell6" forIndexPath:indexPath];
            cell.backgroundColor = BGLightGrayColor;RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
            cell.baseView.backgroundColor = WhiteColor;
            cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"name"]];
            cell.detailLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"intro"]];
            cell.typeLabel.text = [NSString stringWithFormat:@"[%@]",self.weekHotReadArray[indexPath.item][@"categoryName"]];
            return cell;
        }else if (indexPath.section == 5) {
            BookstoreCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell1" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = WhiteColor;
            cell.leftLabel.text = @"畅销小说";
            return cell;
        }else {
            BookstoreCollectionViewCell4 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell4" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = WhiteColor;
            cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.sellingArray[indexPath.item][@"name"]];
            NSString *imageUrl = [NSString stringWithFormat:@"%@",self.sellingArray[indexPath.item][@"cover"]];
            SDWebImageWith(cell.imageV, imageUrl);
            return cell;
        }
    }else{
        if (indexPath.section == 0) {
            BookstoreCollectionViewCell0 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell0" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = WhiteColor;
            SDCycleScrollView *scrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, cell.size.width-30*AdaptRatio, cell.size.height-20*AdaptRatio) shouldInfiniteLoop:YES imageNamesGroup:self.bannerImageArray];
            scrollView.backgroundColor = ClearColor;
            scrollView.autoScrollTimeInterval = 2;
            //[scrollView.layer setCornerRadius:10.0f];
            //scrollView.layer.masksToBounds = YES;
            scrollView.pageDotColor = ThemeColor;
            scrollView.delegate = self;
            [cell addSubview:scrollView];
            if (self.bannerImageArray.count<2) {
                scrollView.autoScroll = NO;
            }
            scrollView.layer.masksToBounds = YES;
            scrollView.layer.cornerRadius = 3;
            return cell;
        }else if (indexPath.section == 1) {
            BookstoreCollectionViewCell5 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell5" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = WhiteColor;
            if (indexPath.item == 0) {
                cell.titleLabel.text = @"分类";
                cell.imageV.image = [UIImage imageNamed:@"bookstore_type"];
            }else if (indexPath.item == 1){
                cell.titleLabel.text = @"排行";
                cell.imageV.image = [UIImage imageNamed:@"bookstore_charts"];
            }else{
                cell.titleLabel.text = @"免费";
                cell.imageV.image = [UIImage imageNamed:@"bookstore_free"];
            }
            return cell;
        }else if (indexPath.section == 2) {
            BookstoreCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell1" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = ClearColor;
            cell.leftLabel.text = @"重磅推荐";
            return cell;
        }else if (indexPath.section == 3) {
            BookstoreCollectionViewCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell2" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = ClearColor;
            return cell;
        }else if (indexPath.section == 4) {
            BookstoreCollectionViewCell6 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell6" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = ClearColor;
            cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"name"]];
            cell.detailLabel.text = [NSString stringWithFormat:@"%@",self.weekHotReadArray[indexPath.item][@"intro"]];
            cell.typeLabel.text = [NSString stringWithFormat:@"[%@]",self.weekHotReadArray[indexPath.item][@"categoryName"]];
            return cell;
        }else if (indexPath.section == 5) {
            BookstoreCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell1" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = ClearColor;
            cell.leftLabel.text = @"畅销小说";
            return cell;
        }else {
            BookstoreCollectionViewCell4 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookstoreCollectionViewCell4" forIndexPath:indexPath];
            cell.backgroundColor = WhiteColor;
            cell.baseView.backgroundColor = ClearColor;
            cell.titleLabel.text = [NSString stringWithFormat:@"%@",self.sellingArray[indexPath.item][@"name"]];
            NSString *imageUrl = [NSString stringWithFormat:@"%@",self.sellingArray[indexPath.item][@"cover"]];
            SDWebImageWith(cell.imageV, imageUrl);
            return cell;
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectIndex == 1) {
        if (indexPath.section == 2) {
            NSDictionary *dict = self.recommenndArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 4){
            NSDictionary *dict = self.weekHotReadArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 6){
            NSDictionary *dict = self.sellingArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }else if (self.selectIndex == 2){
        if (indexPath.section == 1) {
            if (indexPath.item == 0) {
                SaxTypeViewController *vc = [[SaxTypeViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.saxStr = @"女频";
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.item == 1){
                RankViewController *vc = [[RankViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                FreeViewController *vc = [[FreeViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if (indexPath.section == 3){
            NSDictionary *dict = self.recommenndArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 4){
            NSDictionary *dict = self.weekHotReadArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 6){
            NSDictionary *dict = self.sellingArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
        if (indexPath.section == 1) {
            if (indexPath.item == 0) {
                SaxTypeViewController *vc = [[SaxTypeViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.saxStr = @"男频";
                [self.navigationController pushViewController:vc animated:YES];
            }else if (indexPath.item == 1){
                RankViewController *vc = [[RankViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                FreeViewController *vc = [[FreeViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if (indexPath.section == 3){
            NSDictionary *dict = self.recommenndArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 4){
            NSDictionary *dict = self.weekHotReadArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 6){
            NSDictionary *dict = self.sellingArray[indexPath.item];
            BookDetailViewController *vc = [[BookDetailViewController alloc] init];
            vc.bookId = [NSString stringWithFormat:@"%@",dict[@"id"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectIndex == 1) {
        if (indexPath.section == 0) {
            return CGSizeMake(DeviceWidth, 160*AdaptRatio);
        }else if (indexPath.section == 1){
            return CGSizeMake(DeviceWidth, 45*AdaptRatio);
        }else if (indexPath.section == 2){
            return CGSizeMake(DeviceWidth/3, 220*AdaptRatio);
        }else if (indexPath.section == 3){
            return CGSizeMake(DeviceWidth, 45*AdaptRatio);
        }else if (indexPath.section == 4){
            return CGSizeMake(DeviceWidth, 110*AdaptRatio);
        }else if (indexPath.section == 5){
            return CGSizeMake(DeviceWidth, 45*AdaptRatio);
        }else{
            return CGSizeMake(DeviceWidth/3, 190*AdaptRatio);
        }
    }else if (self.selectIndex == 2){
        if (indexPath.section == 0) {
            return CGSizeMake(DeviceWidth, 160*AdaptRatio);
        }else if (indexPath.section == 1){
            return CGSizeMake(DeviceWidth/3, 80*AdaptRatio);
        }else if (indexPath.section == 2){
            return CGSizeMake(DeviceWidth/3, 45*AdaptRatio);
        }else if (indexPath.section == 3){
            return CGSizeMake(DeviceWidth/3, 220*AdaptRatio);
        }else if (indexPath.section == 4){
            return CGSizeMake(DeviceWidth, 65*AdaptRatio);
        }else if (indexPath.section == 5){
            return CGSizeMake(DeviceWidth, 45*AdaptRatio);
        }else{
            return CGSizeMake(DeviceWidth/3, 190*AdaptRatio);
        }
    }else{
        if (indexPath.section == 0) {
            return CGSizeMake(DeviceWidth, 160*AdaptRatio);
        }else if (indexPath.section == 1){
            return CGSizeMake(DeviceWidth/3, 80*AdaptRatio);
        }else if (indexPath.section == 2){
            return CGSizeMake(DeviceWidth/3, 45*AdaptRatio);
        }else if (indexPath.section == 3){
            return CGSizeMake(DeviceWidth/3, 220*AdaptRatio);
        }else if (indexPath.section == 4){
            return CGSizeMake(DeviceWidth, 65*AdaptRatio);
        }else if (indexPath.section == 5){
            return CGSizeMake(DeviceWidth, 45*AdaptRatio);
        }else{
            return CGSizeMake(DeviceWidth/3, 190*AdaptRatio);
        }
    }
    
}





- (void)topView
{
    self.baseView = [[UIView alloc] init];
    //self.baseView.backgroundColor = CyanColor;
    self.baseView.frame = CGRectMake(50*AdaptRatio, [Tools getStatusBarHeight], DeviceWidth-100*AdaptRatio, 50*AdaptRatio);
    [self.view addSubview:self.baseView];
    
    UIButton *investBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //investBtn.backgroundColor = RedColor;
    investBtn.frame = CGRectMake(0, 10*AdaptRatio, 80*AdaptRatio, 30*AdaptRatio);
    investBtn.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:17*AdaptRatio];
    [investBtn setTitle:@"精选" forState:UIControlStateNormal];
    [investBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [self.baseView addSubview:investBtn];
    investBtn.tag = 301;
    [investBtn addTarget:self action:@selector(investBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    //self.recommendMoreBtn.layer.masksToBounds = YES;
    //self.recommendMoreBtn.layer.cornerRadius = 30*AdaptRatio/2;
    //[investBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    UIButton *borrowMoneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //borrowMoneyBtn.backgroundColor = BlueColor;
    borrowMoneyBtn.frame = CGRectMake(80*AdaptRatio+(DeviceWidth-340*AdaptRatio)/2, 10*AdaptRatio, 80*AdaptRatio, 30*AdaptRatio);
    borrowMoneyBtn.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:17*AdaptRatio];
    [borrowMoneyBtn setTitle:@"女生" forState:UIControlStateNormal];
    [borrowMoneyBtn setTitleColor:LightGrayColor forState:UIControlStateNormal];
    [self.baseView addSubview:borrowMoneyBtn];
    borrowMoneyBtn.tag = 302;
    [borrowMoneyBtn addTarget:self action:@selector(borrowMoneyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *myCreditBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //myCreditBtn.backgroundColor = RedColor;
    myCreditBtn.frame = CGRectMake(80*AdaptRatio*2+(DeviceWidth-340*AdaptRatio)/2*2, 10*AdaptRatio, 80*AdaptRatio, 30*AdaptRatio);
    myCreditBtn.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:17*AdaptRatio];
    [myCreditBtn setTitle:@"男生" forState:UIControlStateNormal];
    [myCreditBtn setTitleColor:LightGrayColor forState:UIControlStateNormal];
    [self.baseView addSubview:myCreditBtn];
    myCreditBtn.tag = 303;
    [myCreditBtn addTarget:self action:@selector(myCreditBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = BlackColor;
    self.lineView.frame = CGRectMake(27*AdaptRatio, self.baseView.size.height-3*AdaptRatio, 26*AdaptRatio, 3*AdaptRatio);
    [self.baseView addSubview:self.lineView];
    self.lineView.layer.cornerRadius = 3*AdaptRatio/2;
    self.lineView.layer.masksToBounds = YES;
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //searchBtn.backgroundColor = GreenColor;
    searchBtn.frame = CGRectMake(DeviceWidth-40*AdaptRatio, [Tools getStatusBarHeight]+12.5*AdaptRatio, 25*AdaptRatio, 25*AdaptRatio);
    searchBtn.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:17*AdaptRatio];
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"shop_search"] forState:UIControlStateNormal];
    [searchBtn setTitleColor:LightGrayColor forState:UIControlStateNormal];
    [self.view addSubview:searchBtn];
    [searchBtn addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)searchBtnClick:(UIButton *)sender
{
    BookStoreSearchTableViewController *vc = [[BookStoreSearchTableViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)investBtnClick:(UIButton *)sender
{
    [sender setTitleColor:BlackColor forState:UIControlStateNormal];
    UIButton *btn2 = [self.view viewWithTag:302];
    [btn2 setTitleColor:LightGrayColor forState:UIControlStateNormal];
    UIButton *btn3 = [self.view viewWithTag:303];
    [btn3 setTitleColor:LightGrayColor forState:UIControlStateNormal];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.lineView.frame = CGRectMake(27*AdaptRatio, 45*AdaptRatio, 26*AdaptRatio, 3*AdaptRatio);
    [UIView commitAnimations];
    self.selectIndex = 1;
    //[self.collectionView reloadData];
    
    self.isHaveMore = YES;
    self.pageNumber = 1;
    self.sellingArray = [NSMutableArray array];
    
    //请求 精选
    [self request];
    
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)borrowMoneyBtnClick:(UIButton *)sender
{
    [sender setTitleColor:BlackColor forState:UIControlStateNormal];
    UIButton *btn1 = [self.view viewWithTag:301];
    [btn1 setTitleColor:LightGrayColor forState:UIControlStateNormal];
    UIButton *btn3 = [self.view viewWithTag:303];
    [btn3 setTitleColor:LightGrayColor forState:UIControlStateNormal];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.lineView.frame = CGRectMake(80*AdaptRatio+(DeviceWidth-340*AdaptRatio)/2+27*AdaptRatio, 45*AdaptRatio, 26*AdaptRatio, 3*AdaptRatio);
    [UIView commitAnimations];
    self.selectIndex = 2;
    //[self.collectionView reloadData];
    
    self.isHaveMore = YES;
    self.pageNumber = 1;
    self.sellingArray = [NSMutableArray array];
    
    //请求 女生
    [self request];
    
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)myCreditBtnClick:(UIButton *)sender
{
    [sender setTitleColor:BlackColor forState:UIControlStateNormal];
    UIButton *btn2 = [self.view viewWithTag:302];
    [btn2 setTitleColor:LightGrayColor forState:UIControlStateNormal];
    UIButton *btn1 = [self.view viewWithTag:301];
    [btn1 setTitleColor:LightGrayColor forState:UIControlStateNormal];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.lineView.frame = CGRectMake(80*AdaptRatio*2+(DeviceWidth-340*AdaptRatio)/2*2+27*AdaptRatio, 45*AdaptRatio, 26*AdaptRatio, 3*AdaptRatio);
    [UIView commitAnimations];
    self.selectIndex = 3;
    //[self.collectionView reloadData];
    
    self.isHaveMore = YES;
    self.pageNumber = 1;
    self.sellingArray = [NSMutableArray array];
    
    //请求 男生
    [self request];
    
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


@end
