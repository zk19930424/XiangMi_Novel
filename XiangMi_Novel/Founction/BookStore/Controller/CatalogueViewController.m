//
//  CatalogueViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/15.
//  Copyright © 2019 zk. All rights reserved.
//

#import "CatalogueViewController.h"
#import "CatalogueTableViewCell.h"

@interface CatalogueViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,assign) NSInteger readIndex;
@property (nonatomic,assign) BOOL isHaveMore;
@property (nonatomic,assign) NSInteger pageNumber;
@end

@implementation CatalogueViewController

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, AdaptNavigationHeight, DeviceWidth, DeviceHeight-AdaptNavigationHeight-SafeAreaBottomHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
        
        [self.tableView registerClass:[CatalogueTableViewCell class] forCellReuseIdentifier:@"CatalogueTableViewCell"];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseUI:@"目录"];
    
    self.isHaveMore = YES;
    self.pageNumber = 1;
    self.readIndex = 50;
    self.tableView.backgroundColor = BGLightGrayColor;
    [self setRefreshHeaderFooter];
    
    //[self refreshingTable];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.readIndex >5) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.readIndex-5 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (void)refreshingTable
{
    __weak UITableView *tableView = _tableView;
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[self addPage:@"pastpPage"];
            [tableView.mj_header endRefreshing];
        });
    }];
    
    tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[self addPage:@"futurePage"];
            [tableView.mj_footer endRefreshing];
        });
    }];
}

- (void)setRefreshHeaderFooter
{
    __weak UITableView *tableView = _tableView;
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[self addPage:@"pastpPage"];
            [tableView.mj_header endRefreshing];
        });
    }];
    tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[self addPage:@"futurePage"];
            [tableView.mj_footer endRefreshing];
        });
    }];
}

- (void)footerClick
{
    [self.tableView.mj_footer endRefreshing];
    if (self.isHaveMore == NO) {
        [Tools showMessage:@"没有更多数据"];
        return;
    }
    self.pageNumber++;
    [self requestForCatalogueList];
}

- (void)requestForCatalogueList
{
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"post" url:@"" loadStr:@"" param:@{} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 100;//array
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CatalogueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CatalogueTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
    cell.titleLabel.text = [NSString stringWithFormat:@"第%ld章 %@",indexPath.row+1,@"人生自古谁无死"];
    if (indexPath.row+1>self.readIndex) {
        cell.titleLabel.textColor = GrayColor;
    }else{
        cell.titleLabel.textColor = BlackColor;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45*AdaptRatio;
}


- (void)baseUI:(NSString *)titleStr
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = WhiteColor;
    self.title = titleStr;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self.view addSubview:backBtn];
    //backBtn.backgroundColor = RedColor;
    backBtn.frame = CGRectMake(0,0,25,25);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backBtnItem;
}

- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
