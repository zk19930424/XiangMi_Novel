//
//  RankViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/16.
//  Copyright © 2019 zk. All rights reserved.
//

#import "RankViewController.h"
#import "RankCollectionViewCell.h"

@interface RankViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UIScrollView *typeScrollView;
@property (nonatomic,strong) UIView *typeLineView;
@property (nonatomic,assign) NSInteger selectIndex;

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation RankViewController

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(70*AdaptRatio, AdaptNavigationHeight, DeviceWidth-(70+0)*AdaptRatio, DeviceHeight-SafeAreaBottomHeight-AdaptNavigationHeight) collectionViewLayout:layout];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.view addSubview:self.collectionView];
        self.collectionView.backgroundColor = ClearColor;
        self.collectionView.bounces = YES;
        self.collectionView.showsVerticalScrollIndicator = NO;
        
        [self.collectionView registerClass:[RankCollectionViewCell class] forCellWithReuseIdentifier:@"RankCollectionViewCell"];
        
    }
    return _collectionView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseUI:@"排行"];
    
    self.selectIndex = 1;
    [self addLeftScrollView:@[@"点击榜",@"畅销榜",@"推荐榜",@"更新榜"]];
    
    self.dataArray = [NSMutableArray array];
    self.collectionView.backgroundColor = WhiteColor;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 9;//array
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    RankCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RankCollectionViewCell" forIndexPath:indexPath];
    //cell.backgroundColor = RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
    //SDWebImageWith(cell.imageV,@"");
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionView.size.width, 85*AdaptRatio);
}


- (void)addLeftScrollView:(NSArray *)array
{
    self.typeScrollView = [[UIScrollView alloc] init];
    self.typeScrollView.backgroundColor = BGLightGrayColor;
    self.typeScrollView.frame = CGRectMake(0, AdaptNavigationHeight, 70*AdaptRatio, DeviceHeight-SafeAreaBottomHeight-AdaptNavigationHeight-0*AdaptRatio);
    self.typeScrollView.contentSize = CGSizeMake(70*AdaptRatio, array.count*40*AdaptRatio+10*AdaptRatio);
    self.typeScrollView.bounces = YES;
    self.typeScrollView.alwaysBounceVertical = YES;//水平
    self.typeScrollView.decelerationRate = 0.1;//减速速度
    self.typeScrollView.showsHorizontalScrollIndicator = NO;
    self.typeScrollView.showsVerticalScrollIndicator = NO;
    self.typeScrollView.directionalLockEnabled = YES;
    [self.view addSubview:self.typeScrollView];
    self.typeScrollView.scrollEnabled = NO;
    
    for (int i=0; i<array.count; i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0*AdaptRatio,70*AdaptRatio*i,AdaptRatio*70,70*AdaptRatio)];
        //view.backgroundColor = RGB(arc4random()%255*1.0, arc4random()%255*1.0, arc4random()%255*1.0);
        //view.layer.cornerRadius = 5;
        //view.layer.masksToBounds = YES;
        [self.typeScrollView addSubview:view];
        
        UIButton *bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [view addSubview:bottomBtn];
        //bottomBtn.backgroundColor = RedColor;
        bottomBtn.frame = CGRectMake(0,0,70*AdaptRatio,70*AdaptRatio);
        bottomBtn.tag = 400+i;
        [bottomBtn addTarget:self action:@selector(bottomBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bottomBtn setTitleColor:BlackColor forState:UIControlStateNormal];
        [bottomBtn setTitleColor:ThemeColor forState:UIControlStateSelected];
        [bottomBtn setTitle:array[i] forState:UIControlStateNormal];
        bottomBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
        if (self.selectIndex == i+1) {
            [bottomBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
            bottomBtn.backgroundColor = WhiteColor;
        }
    }
    
    self.typeLineView = [[UIView alloc] initWithFrame:CGRectMake(0*AdaptRatio,70*AdaptRatio*(self.selectIndex-1),3*AdaptRatio,70*AdaptRatio)];
    self.typeLineView.backgroundColor = ThemeColor;
    [self.typeScrollView addSubview:self.typeLineView];
    
}

- (void)bottomBtnClick:(UIButton *)sender
{
    for (int i = 0; i < @[@"点击榜",@"畅销榜",@"推荐榜",@"更新榜"].count; i++) {
        UIButton *btn = [self.typeScrollView viewWithTag:i+400];
        [btn setTitleColor:BlackColor forState:UIControlStateNormal];
        btn.backgroundColor = BGLightGrayColor;
    }
    [sender setTitleColor:ThemeColor forState:UIControlStateNormal];
    sender.backgroundColor = WhiteColor;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.typeLineView.frame = CGRectMake(0*AdaptRatio,70*AdaptRatio*(sender.tag-400),3*AdaptRatio,70*AdaptRatio);
    [UIView commitAnimations];
    self.selectIndex = sender.tag-400;
    //[self.collectionView reloadData];
}



- (void)baseUI:(NSString *)titleStr
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = WhiteColor;
    self.title = titleStr;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self.view addSubview:backBtn];
    //backBtn.backgroundColor = RedColor;
    backBtn.frame = CGRectMake(0,0,25,25);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backBtnItem;
}

- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
