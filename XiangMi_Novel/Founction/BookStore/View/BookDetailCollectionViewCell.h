//
//  BookDetailCollectionViewCell.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/15.
//  Copyright © 2019 zk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWStarView.h"
NS_ASSUME_NONNULL_BEGIN

@interface BookDetailCollectionViewCell0 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *autherLabel;
@property (nonatomic,strong) UILabel *scoreLabel;
@property (nonatomic,strong) JWStarView *starView;//评星
@property (nonatomic,strong) UILabel *typeLabel;
@property (nonatomic,strong) UILabel *wordNumberLabel;
@property (nonatomic,strong) UILabel *stateLabel;
@property (nonatomic,strong) UILabel *clickLabel1;
@property (nonatomic,strong) UILabel *clickTitleLabel1;
@property (nonatomic,strong) UILabel *clickLabel2;
@property (nonatomic,strong) UILabel *clickTitleLabel2;
@property (nonatomic,strong) UILabel *clickLabel3;
@property (nonatomic,strong) UILabel *clickTitleLabel3;
@end

@interface BookDetailCollectionViewCell1 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *leftLabel;
@end


@interface BookDetailCollectionViewCell2 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *titleLabel;
- (CGFloat)fitUI:(NSString *)string;
@end


@interface BookDetailCollectionViewCell3 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *leftLabel;
@property (nonatomic,strong) UILabel *centerLabel;
@property (nonatomic,strong) UIImageView *arrowImageV;
@end


@interface BookDetailCollectionViewCell4 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleLabel;
@end


NS_ASSUME_NONNULL_END
