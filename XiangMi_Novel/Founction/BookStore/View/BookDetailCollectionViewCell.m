//
//  BookDetailCollectionViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/15.
//  Copyright © 2019 zk. All rights reserved.
//

#import "BookDetailCollectionViewCell.h"

@implementation BookDetailCollectionViewCell0
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.imageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.imageV];
        self.imageV.frame = CGRectMake(15*AdaptRatio, 20*AdaptRatio, 105*AdaptRatio, 140*AdaptRatio);
        //self.imageV.backgroundColor = RedColor;
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(120*AdaptRatio+15*AdaptRatio, 20*AdaptRatio+10*AdaptRatio, self.baseView.size.width-135*AdaptRatio, 30*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = BlueColor;
        self.titleLabel.text = @"一世倾城";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangBold] size:22*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
        
        self.autherLabel = [[UILabel alloc] initWithFrame:CGRectMake(120*AdaptRatio+15*AdaptRatio, 55*AdaptRatio+10*AdaptRatio, self.baseView.size.width-135*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.autherLabel];
        //self.autherLabel.backgroundColor = RedColor;
        self.autherLabel.text = @"张爱玲 著";
        self.autherLabel.textAlignment = NSTextAlignmentLeft;
        self.autherLabel.textColor = GrayColor;
        self.autherLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:13*AdaptRatio];
        self.autherLabel.numberOfLines = 0;
        
        self.scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(120*AdaptRatio+15*AdaptRatio, 80*AdaptRatio+10*AdaptRatio, 30*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.scoreLabel];
        //self.scoreLabel.backgroundColor = RedColor;
        self.scoreLabel.text = @"评分";
        self.scoreLabel.textAlignment = NSTextAlignmentLeft;
        self.scoreLabel.textColor = LightGrayColor;
        self.scoreLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.scoreLabel.numberOfLines = 0;
        
        self.starView = [[JWStarView alloc] initWithFrame:CGRectMake(150*AdaptRatio+15*AdaptRatio, 85*AdaptRatio+10*AdaptRatio, 80*AdaptRatio, 10*AdaptRatio)];
        self.starView.currentScore = 4;
        self.starView.hidden = NO;
        self.starView.rateStyle = WholeStar;
        self.starView.isIndicator = YES;
        //self.starView.backgroundColor = RedColor;
        [self.baseView addSubview:self.starView];
        
        self.typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(120*AdaptRatio+15*AdaptRatio, 110*AdaptRatio+10*AdaptRatio, 60*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.typeLabel];
        self.typeLabel.backgroundColor = UIColorFromRGB(0xf0f3f6);
        self.typeLabel.text = @"";
        self.typeLabel.textAlignment = NSTextAlignmentCenter;
        self.typeLabel.textColor = UIColorFromRGB(0x848faa);
        self.typeLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.typeLabel.numberOfLines = 0;
        self.typeLabel.layer.masksToBounds = YES;
        self.typeLabel.layer.cornerRadius = 20*AdaptRatio/2;
        
        self.wordNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(190*AdaptRatio+15*AdaptRatio, 110*AdaptRatio+10*AdaptRatio, 60*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.wordNumberLabel];
        self.wordNumberLabel.backgroundColor = UIColorFromRGB(0xf0f3f6);
        self.wordNumberLabel.text = @"";
        self.wordNumberLabel.textAlignment = NSTextAlignmentCenter;
        self.wordNumberLabel.textColor = UIColorFromRGB(0x848faa);
        self.wordNumberLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.wordNumberLabel.numberOfLines = 0;
        self.wordNumberLabel.layer.masksToBounds = YES;
        self.wordNumberLabel.layer.cornerRadius = 20*AdaptRatio/2;
        
        self.stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(260*AdaptRatio+15*AdaptRatio, 110*AdaptRatio+10*AdaptRatio, 60*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.stateLabel];
        self.stateLabel.backgroundColor = UIColorFromRGB(0xf0f3f6);
        self.stateLabel.text = @"";
        self.stateLabel.textAlignment = NSTextAlignmentCenter;
        self.stateLabel.textColor = UIColorFromRGB(0x848faa);
        self.stateLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.stateLabel.numberOfLines = 0;
        self.stateLabel.layer.masksToBounds = YES;
        self.stateLabel.layer.cornerRadius = 20*AdaptRatio/2;
        
        self.clickLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0*AdaptRatio, 175*AdaptRatio, self.baseView.size.width/3, 30*AdaptRatio)];
        [self.baseView addSubview:self.clickLabel1];
        //self.clickLabel1.backgroundColor = RedColor;
        self.clickLabel1.text = @"";
        self.clickLabel1.textAlignment = NSTextAlignmentCenter;
        self.clickLabel1.textColor = BlackColor;
        self.clickLabel1.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:20*AdaptRatio];
        self.clickLabel1.numberOfLines = 0;
        
        self.clickLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/3, 175*AdaptRatio, self.baseView.size.width/3, 30*AdaptRatio)];
        [self.baseView addSubview:self.clickLabel2];
        //self.clickLabel2.backgroundColor = GreenColor;
        self.clickLabel2.text = @"";
        self.clickLabel2.textAlignment = NSTextAlignmentCenter;
        self.clickLabel2.textColor = BlackColor;
        self.clickLabel2.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:20*AdaptRatio];
        self.clickLabel2.numberOfLines = 0;
        
        self.clickLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width*2/3, 175*AdaptRatio, self.baseView.size.width/3, 30*AdaptRatio)];
        [self.baseView addSubview:self.clickLabel3];
        //self.clickLabel3.backgroundColor = RedColor;
        self.clickLabel3.text = @"";
        self.clickLabel3.textAlignment = NSTextAlignmentCenter;
        self.clickLabel3.textColor = BlackColor;
        self.clickLabel3.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:20*AdaptRatio];
        self.clickLabel3.numberOfLines = 0;
        
        self.clickTitleLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0*AdaptRatio, 205*AdaptRatio, self.baseView.size.width/3, 20*AdaptRatio)];
        [self.baseView addSubview:self.clickTitleLabel1];
        //self.clickTitleLabel1.backgroundColor = RedColor;
        self.clickTitleLabel1.text = @"周点击";
        self.clickTitleLabel1.textAlignment = NSTextAlignmentCenter;
        self.clickTitleLabel1.textColor = LightGrayColor;
        self.clickTitleLabel1.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:11*AdaptRatio];
        self.clickTitleLabel1.numberOfLines = 0;
        
        self.clickTitleLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/3, 205*AdaptRatio, self.baseView.size.width/3, 20*AdaptRatio)];
        [self.baseView addSubview:self.clickTitleLabel2];
        //self.clickTitleLabel2.backgroundColor = GreenColor;
        self.clickTitleLabel2.text = @"月点击";
        self.clickTitleLabel2.textAlignment = NSTextAlignmentCenter;
        self.clickTitleLabel2.textColor = LightGrayColor;
        self.clickTitleLabel2.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.clickTitleLabel2.numberOfLines = 0;
        
        self.clickTitleLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width*2/3, 205*AdaptRatio, self.baseView.size.width/3, 20*AdaptRatio)];
        [self.baseView addSubview:self.clickTitleLabel3];
        //self.clickTitleLabel3.backgroundColor = RedColor;
        self.clickTitleLabel3.text = @"总点击";
        self.clickTitleLabel3.textAlignment = NSTextAlignmentCenter;
        self.clickTitleLabel3.textColor = LightGrayColor;
        self.clickTitleLabel3.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.clickTitleLabel3.numberOfLines = 0;
        
    }
    return self;
}
@end

@implementation BookDetailCollectionViewCell1
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 10*AdaptRatio, self.size.width, self.size.height-10*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, 100*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.leftLabel];
        //self.leftLabel.backgroundColor = RedColor;
        self.leftLabel.text = @"简介";
        self.leftLabel.textAlignment = NSTextAlignmentLeft;
        self.leftLabel.textColor = BlackColor;
        self.leftLabel.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:18*AdaptRatio];
    }
    return self;
}
@end

@implementation BookDetailCollectionViewCell2
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0*AdaptRatio, self.size.width, self.size.height-0*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, self.baseView.size.width-30*AdaptRatio, 50*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        self.titleLabel.backgroundColor = WhiteColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = GrayColor;
        self.titleLabel.font = [UIFont systemFontOfSize:13*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
    }
    return self;
}
//动态高度
- (CGFloat)fitUI:(NSString *)string
{
    CGFloat height = [Tools getDynamicHeightWithWidth:self.baseView.size.width-30*AdaptRatio fontSize:13*AdaptRatio Text:string numOfLins:0 spacing:0];
    self.titleLabel.frame = CGRectMake(15*AdaptRatio, 10*AdaptRatio, self.baseView.size.width-30*AdaptRatio, height);
    self.titleLabel.text = string;
    self.baseView.frame = CGRectMake(0, 10*AdaptRatio, DeviceWidth, height);
    return height+20*AdaptRatio;
}
@end

@implementation BookDetailCollectionViewCell3
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0*AdaptRatio, self.size.width, self.size.height-0*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.frame = CGRectMake(0, 0*AdaptRatio, self.baseView.size.width, 0.5*AdaptRatio);
        lineView.backgroundColor = LightGrayColor;
        lineView.alpha = 0.3;
        [self.baseView addSubview:lineView];
        
        self.leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, 40*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.leftLabel];
        //self.leftLabel.backgroundColor = OrangeColor;
        self.leftLabel.text = @"目录";
        self.leftLabel.textAlignment = NSTextAlignmentLeft;
        self.leftLabel.textColor = BlackColor;
        self.leftLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
        self.leftLabel.numberOfLines = 0;
        
        self.centerLabel = [[UILabel alloc] initWithFrame:CGRectMake(70*AdaptRatio, 10*AdaptRatio, self.baseView.size.width-110*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.centerLabel];
        //self.centerLabel.backgroundColor = GreenColor;
        self.centerLabel.text = @"";
        self.centerLabel.textAlignment = NSTextAlignmentLeft;
        self.centerLabel.textColor = ThemeColor;
        self.centerLabel.font = [UIFont systemFontOfSize:13*AdaptRatio];
        self.centerLabel.numberOfLines = 0;
        
        self.arrowImageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.arrowImageV];
        self.arrowImageV.frame = CGRectMake(self.baseView.size.width-30*AdaptRatio, 12.5*AdaptRatio, 15*AdaptRatio, 15*AdaptRatio);
        //self.arrowImageV.backgroundColor = RedColor;
        self.arrowImageV.image = [UIImage imageNamed:@"button_open"];
    }
    return self;
}
@end

@implementation BookDetailCollectionViewCell4
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height-10*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.imageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.imageV];
        self.imageV.frame = CGRectMake(self.baseView.size.width/2-45*AdaptRatio, 15*AdaptRatio, 90*AdaptRatio, 125*AdaptRatio);
        //self.imageV.backgroundColor = RedColor;
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/2-45*AdaptRatio, 140*AdaptRatio, 90*AdaptRatio, 40*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = BlueColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:14*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
    }
    return self;
}
@end
