//
//  BookstoreCollectionViewCell.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookstoreCollectionViewCell0 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleLabel;
@end


@interface BookstoreCollectionViewCell1 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *leftLabel;
@property (nonatomic,strong) UILabel *rightLabel;
@end


@interface BookstoreCollectionViewCell2 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *autherLabel;

@end


@interface BookstoreCollectionViewCell3 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *headerImageView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *autherLabel;
@property (nonatomic,strong) UILabel *typeLabel;
@property (nonatomic,strong) UILabel *detailLabel;
@end


@interface BookstoreCollectionViewCell4 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleLabel;
@end


@interface BookstoreCollectionViewCell5 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleLabel;
@end


@interface BookstoreCollectionViewCell6 : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *typeLabel;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *detailLabel;
@end


NS_ASSUME_NONNULL_END
