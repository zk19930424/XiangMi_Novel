//
//  BookstoreCollectionViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import "BookstoreCollectionViewCell.h"

@implementation BookstoreCollectionViewCell0
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
//        self.imageV = [[UIImageView alloc] init];
//        //[self.baseView addSubview:self.imageV];
//        self.imageV.frame = CGRectMake(15*AdaptRatio, 10*AdaptRatio, self.baseView.size.width-30*AdaptRatio, 100*AdaptRatio);
//        self.imageV.backgroundColor = RedColor;
//        self.imageV.layer.masksToBounds = YES;
//        self.imageV.layer.cornerRadius = 3;
    }
    return self;
}
@end



@implementation BookstoreCollectionViewCell1
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        self.leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*AdaptRatio, 15*AdaptRatio, 100*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.leftLabel];
        //self.leftLabel.backgroundColor = RedColor;
        self.leftLabel.text = @"";
        self.leftLabel.textAlignment = NSTextAlignmentLeft;
        self.leftLabel.textColor = BlackColor;
        self.leftLabel.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:19*AdaptRatio];
        
        self.rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width-65*AdaptRatio, 15*AdaptRatio, 50*AdaptRatio, 20*AdaptRatio)];
        //[self.baseView addSubview:self.rightLabel];
        //self.rightLabel.backgroundColor = RedColor;
        self.rightLabel.text = @"更多";
        self.rightLabel.textAlignment = NSTextAlignmentRight;
        self.rightLabel.textColor = ThemeColor;
        self.rightLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*AdaptRatio];
    }
    return self;
}
@end



@implementation BookstoreCollectionViewCell2
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        self.imageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.imageV];
        self.imageV.frame = CGRectMake(self.baseView.size.width/2-50*AdaptRatio, 15*AdaptRatio, 100*AdaptRatio, 125*AdaptRatio);
        //self.imageV.backgroundColor = RedColor;
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/2-50*AdaptRatio, 140*AdaptRatio, 100*AdaptRatio, 35*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = BlueColor;
        self.titleLabel.text = @"哈哈哈哈哈哈哈哈哈";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
        
        self.autherLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/2-50*AdaptRatio, 175*AdaptRatio, 100*AdaptRatio, 35*AdaptRatio)];
        [self.baseView addSubview:self.autherLabel];
        //self.autherLabel.backgroundColor = RedColor;
        self.autherLabel.text = @"哈哈哈哈哈哈哈哈哈";
        self.autherLabel.textAlignment = NSTextAlignmentLeft;
        self.autherLabel.textColor = GrayColor;
        self.autherLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.autherLabel.numberOfLines = 0;
    }
    return self;
}
@end




@implementation BookstoreCollectionViewCell3
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        self.headerImageView = [[UIImageView alloc] init];
        [self.baseView addSubview:self.headerImageView];
        self.headerImageView.frame = CGRectMake(15*AdaptRatio, 10*AdaptRatio, 65*AdaptRatio, 90*AdaptRatio);
        //self.headerImageView.backgroundColor = RedColor;
        self.headerImageView.layer.masksToBounds = YES;
        self.headerImageView.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(95*AdaptRatio, 10*AdaptRatio, self.baseView.size.width-110*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = GreenColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:16*AdaptRatio];
        
        self.autherLabel = [[UILabel alloc] initWithFrame:CGRectMake(95*AdaptRatio, 35*AdaptRatio, self.baseView.size.width-170*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.autherLabel];
        //self.autherLabel.backgroundColor = RedColor;
        self.autherLabel.text = @"";
        self.autherLabel.textAlignment = NSTextAlignmentLeft;
        self.autherLabel.textColor = GrayColor;
        self.autherLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        
        self.typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width-65*AdaptRatio, 32.5*AdaptRatio, 40*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.typeLabel];
        //self.typeLabel.backgroundColor = BlueColor;
        self.typeLabel.text = @"言情";
        self.typeLabel.textAlignment = NSTextAlignmentCenter;
        self.typeLabel.textColor = ThemeColor;
        self.typeLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:11*AdaptRatio];
        self.typeLabel.layer.masksToBounds = YES;
        self.typeLabel.layer.cornerRadius = 20*AdaptRatio/2;
        self.typeLabel.layer.borderWidth = 0.5*AdaptRatio;
        self.typeLabel.layer.borderColor = ThemeColor.CGColor;
        
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(95*AdaptRatio, 55*AdaptRatio, self.baseView.size.width-110*AdaptRatio, 40*AdaptRatio)];
        [self.baseView addSubview:self.detailLabel];
        //self.detailLabel.backgroundColor = PurpleColor;
        self.detailLabel.text = @"";
        self.detailLabel.textAlignment = NSTextAlignmentLeft;
        self.detailLabel.textColor = GrayColor;
        self.detailLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.detailLabel.numberOfLines = 0;
    }
    return self;
}
@end



@implementation BookstoreCollectionViewCell4
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        self.imageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.imageV];
        self.imageV.frame = CGRectMake(self.baseView.size.width/2-45*AdaptRatio, 15*AdaptRatio, 90*AdaptRatio, 125*AdaptRatio);
        //self.imageV.backgroundColor = RedColor;
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/2-45*AdaptRatio, 140*AdaptRatio, 90*AdaptRatio, 40*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = BlueColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:14*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
    }
    return self;
}
@end



@implementation BookstoreCollectionViewCell5
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        self.imageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.imageV];
        self.imageV.frame = CGRectMake(self.baseView.size.width/2-17.5*AdaptRatio, 10*AdaptRatio, 35*AdaptRatio, 35*AdaptRatio);
        //self.imageV.backgroundColor = RedColor;
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/2-17.5*AdaptRatio, 45*AdaptRatio, 35*AdaptRatio, 25*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = BlueColor;
        self.titleLabel.text = @"分类";
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
    }
    return self;
}
@end



@implementation BookstoreCollectionViewCell6
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(15*AdaptRatio, 0, self.baseView.size.width-30*AdaptRatio, 0.5*AdaptRatio)];
        lineView.alpha = 0.3;
        lineView.backgroundColor = LightGrayColor;
        [self.baseView addSubview:lineView];
        
        self.typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, 40*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.typeLabel];
        //self.typeLabel.backgroundColor = BlueColor;
        self.typeLabel.text = @"";
        self.typeLabel.textAlignment = NSTextAlignmentCenter;
        self.typeLabel.textColor = ThemeColor;
        self.typeLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*AdaptRatio];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70*AdaptRatio, 10*AdaptRatio, self.baseView.size.width-85*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = GreenColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
        
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*AdaptRatio, 35*AdaptRatio, self.baseView.size.width-30*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.detailLabel];
        //self.detailLabel.backgroundColor = PurpleColor;
        self.detailLabel.text = @"";
        self.detailLabel.textAlignment = NSTextAlignmentLeft;
        self.detailLabel.textColor = GrayColor;
        self.detailLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:13*AdaptRatio];
        self.detailLabel.numberOfLines = 0;
    }
    return self;
}
@end


