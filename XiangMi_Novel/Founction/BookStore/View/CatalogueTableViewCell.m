//
//  CatalogueTableViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/15.
//  Copyright © 2019 zk. All rights reserved.
//

#import "CatalogueTableViewCell.h"

@implementation CatalogueTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0*AdaptRatio, DeviceWidth, 45*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        self.baseView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.baseView];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.frame = CGRectMake(15*AdaptRatio, 10*AdaptRatio, DeviceWidth, 25*AdaptRatio);
        //self.titleLabel.backgroundColor = [UIColor redColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.font = [UIFont systemFontOfSize:15*AdaptRatio];
        [self.baseView addSubview:self.titleLabel];
        self.titleLabel.text = @"";
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0*AdaptRatio, 44.5*AdaptRatio, DeviceWidth, 0.5*AdaptRatio)];
        lineView.backgroundColor = LightGrayColor;
        lineView.alpha = 0.3;
        [self.baseView addSubview:lineView];
    }
    return self;
}

@end
