//
//  FreeCollectionViewCell.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/16.
//  Copyright © 2019 zk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FreeCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *progressLabel;
@end

NS_ASSUME_NONNULL_END
