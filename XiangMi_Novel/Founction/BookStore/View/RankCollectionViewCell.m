//
//  RankCollectionViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/16.
//  Copyright © 2019 zk. All rights reserved.
//

#import "RankCollectionViewCell.h"

@implementation RankCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        self.headerImageView = [[UIImageView alloc] init];
        [self.baseView addSubview:self.headerImageView];
        self.headerImageView.frame = CGRectMake(15*AdaptRatio, 10*AdaptRatio, 50*AdaptRatio, 65*AdaptRatio);
        self.headerImageView.backgroundColor = RedColor;
        self.headerImageView.layer.masksToBounds = YES;
        self.headerImageView.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80*AdaptRatio, 10*AdaptRatio, self.baseView.size.width-110*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = GreenColor;
        self.titleLabel.text = @"呵呵";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*AdaptRatio];
        
        self.autherLabel = [[UILabel alloc] initWithFrame:CGRectMake(80*AdaptRatio, 32.5*AdaptRatio, self.baseView.size.width-170*AdaptRatio, 15*AdaptRatio)];
        [self.baseView addSubview:self.autherLabel];
        //self.autherLabel.backgroundColor = RedColor;
        self.autherLabel.text = @"呵呵呵";
        self.autherLabel.textAlignment = NSTextAlignmentLeft;
        self.autherLabel.textColor = GrayColor;
        self.autherLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:13*AdaptRatio];
        
        self.typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(80*AdaptRatio, 55*AdaptRatio, 40*AdaptRatio, 15*AdaptRatio)];
        [self.baseView addSubview:self.typeLabel];
        self.typeLabel.backgroundColor = UIColorFromRGB(0xf0f3f6);
        self.typeLabel.text = @"言情";
        self.typeLabel.textAlignment = NSTextAlignmentCenter;
        self.typeLabel.textColor = UIColorFromRGB(0x848faa);
        self.typeLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:11*AdaptRatio];
        self.typeLabel.layer.masksToBounds = YES;
        self.typeLabel.layer.cornerRadius = 15*AdaptRatio/2;
        //self.typeLabel.layer.borderWidth = 0.5*AdaptRatio;
        //self.typeLabel.layer.borderColor = ThemeColor.CGColor;
    }
    return self;
}
@end
