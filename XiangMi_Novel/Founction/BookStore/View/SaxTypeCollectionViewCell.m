
//
//  SaxTypeCollectionViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/15.
//  Copyright © 2019 zk. All rights reserved.
//

#import "SaxTypeCollectionViewCell.h"

@implementation SaxTypeCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = ClearColor;
        [self.contentView addSubview:self.baseView];
        
        self.imageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.imageV];
        self.imageV.frame = CGRectMake(15*AdaptRatio, 15*AdaptRatio, 45*AdaptRatio, 60*AdaptRatio);
        self.imageV.backgroundColor = RedColor;
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70*AdaptRatio, 35*AdaptRatio, 70*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = BlueColor;
        self.titleLabel.text = @"现代言情";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
    }
    return self;
}
@end
