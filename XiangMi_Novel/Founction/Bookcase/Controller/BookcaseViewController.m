//
//  BookcaseViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 Botao. All rights reserved.
//

#import "BookcaseViewController.h"
#import "BookcaseCollectionViewCell.h"
#import "SigninViewController.h"

@interface BookcaseViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@end

@implementation BookcaseViewController

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, AdaptNavigationHeight+75*AdaptRatio, DeviceWidth, DeviceHeight-49-SafeAreaBottomHeight-AdaptNavigationHeight-75*AdaptRatio) collectionViewLayout:layout];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.view addSubview:self.collectionView];
        self.collectionView.backgroundColor = ClearColor;
        self.collectionView.bounces = YES;
        self.collectionView.showsVerticalScrollIndicator = NO;
        
        [self.collectionView registerClass:[BookcaseCollectionViewCell class] forCellWithReuseIdentifier:@"BookcaseCollectionViewCell"];
    }
    return _collectionView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = WhiteColor;
    self.title = @"我的书架";
    
    [self addTopVoew];
    
    
    self.collectionView.backgroundColor = WhiteColor;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sureLogin"] == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"presentLoginVC" object:nil];
        return;
    }
    
    [self request];
}

//请求 书架
- (void)request
{
    self.dataArray = [NSMutableArray array];
    NSDictionary *dict = @{@"token":[Dao findUserInfo].token,@"start":@"1",@"size":@"500"};
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:[RequestUrl theMyBookcase_get] loadStr:@"0" param:dict Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            self.dataArray = dictionary[@"result"][@"list"];
            if (self.dataArray.count == 0) {
                return ;
            }
            [self.collectionView reloadData];
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.dataArray[indexPath.item];
    BookcaseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookcaseCollectionViewCell" forIndexPath:indexPath];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@",dict[@""]];
    SDWebImageWith(cell.imageV,urlStr);
    if ([[dict[@""] stringValue] isEqualToString:@"0"]) {
        cell.progressLabel.text = @"未读";
    }else{
        cell.progressLabel.text = [NSString stringWithFormat:@"%@/%@",dict[@""],dict[@""]];
    }
    [cell.progressLabel sizeToFit];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(DeviceWidth/3, 215*AdaptRatio);
}

- (void)addTopVoew
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15*AdaptRatio, AdaptNavigationHeight+10*AdaptRatio, DeviceWidth-30*AdaptRatio, 55*AdaptRatio)];
    view.backgroundColor = UIColorFromRGB(0xf8f8f8);
    [self.view addSubview:view];
    view.layer.masksToBounds = YES;
    view.layer.cornerRadius = 3;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*AdaptRatio, 10*AdaptRatio, view.size.width-100*AdaptRatio, 35*AdaptRatio)];
    [view addSubview:titleLabel];
    //titleLabel.backgroundColor = BlueColor;
    titleLabel.text = @"再签到2天将获得现金奖励3元!";
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = BlackColor;
    titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:15*AdaptRatio];
    titleLabel.numberOfLines = 0;
    
    UIButton *signinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [view addSubview:signinBtn];
    signinBtn.backgroundColor = ClearColor;
    signinBtn.frame = CGRectMake(view.size.width-85*AdaptRatio,15*AdaptRatio,70*AdaptRatio,25*AdaptRatio*AdaptRatio);
    [signinBtn setBackgroundImage:[UIImage imageNamed:@"button_sure"] forState:UIControlStateNormal];
    signinBtn.titleLabel.font = [UIFont systemFontOfSize:12*AdaptRatio];
    [signinBtn setTitle:@"立即签到" forState:UIControlStateNormal];
    [signinBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [signinBtn addTarget:self action:@selector(signinBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)signinBtnClick
{
    SigninViewController *vc = [[SigninViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
