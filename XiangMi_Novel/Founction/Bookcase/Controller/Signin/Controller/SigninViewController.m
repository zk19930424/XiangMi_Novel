//
//  SigninViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/15.
//  Copyright © 2019 zk. All rights reserved.
//

#import "SigninViewController.h"

@interface SigninViewController ()

@end

@implementation SigninViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseUI:@"签到"];
}

- (void)baseUI:(NSString *)titleStr
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = WhiteColor;
    self.title = titleStr;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self.view addSubview:backBtn];
    //backBtn.backgroundColor = RedColor;
    backBtn.frame = CGRectMake(0,0,25,25);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backBtnItem;
}

- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
