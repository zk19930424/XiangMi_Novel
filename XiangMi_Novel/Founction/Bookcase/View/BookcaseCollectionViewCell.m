//
//  BookcaseCollectionViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/15.
//  Copyright © 2019 zk. All rights reserved.
//

#import "BookcaseCollectionViewCell.h"

@implementation BookcaseCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.imageV = [[UIImageView alloc] init];
        [self.baseView addSubview:self.imageV];
        self.imageV.frame = CGRectMake(self.baseView.size.width/2-50*AdaptRatio, 15*AdaptRatio, 100*AdaptRatio, 125*AdaptRatio);
        //self.imageV.backgroundColor = RedColor;
        self.imageV.layer.masksToBounds = YES;
        self.imageV.layer.cornerRadius = 3;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/2-50*AdaptRatio, 145*AdaptRatio, 100*AdaptRatio, 35*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = BlueColor;
        self.titleLabel.text = @"我与世界只差";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = BlackColor;
        self.titleLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.titleLabel.numberOfLines = 0;
        
        self.progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.baseView.size.width/2-50*AdaptRatio, 180*AdaptRatio, 100*AdaptRatio, 35*AdaptRatio)];
        [self.baseView addSubview:self.progressLabel];
        //self.progressLabel.backgroundColor = RedColor;
        self.progressLabel.text = @"未读";
        self.progressLabel.textAlignment = NSTextAlignmentLeft;
        self.progressLabel.textColor = GrayColor;
        self.progressLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:12*AdaptRatio];
        self.progressLabel.numberOfLines = 0;
    }
    return self;
}
@end
