//
//  FindViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 Botao. All rights reserved.
//

#import "FindViewController.h"
#import "FindTableViewCell.h"
#import "WelfareViewController.h"

@interface FindViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,assign) NSInteger selectIndex;

@end

@implementation FindViewController

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, AdaptNavigationHeight, DeviceWidth, DeviceHeight-AdaptNavigationHeight-SafeAreaBottomHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
        
        [self.tableView registerClass:[FindTableViewCell class] forCellReuseIdentifier:@"FindTableViewCell"];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"发现";
    self.view.backgroundColor = WhiteColor;
    
    return;
    self.tableView.backgroundColor = BGLightGrayColor;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sureLogin"] == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"presentLoginVC" object:nil];
        return;
    }
}

- (void)request
{
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"post" url:@"" loadStr:@"" param:@{} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FindTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FindTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = BGLightGrayColor;
    if (indexPath.row == 0) {
        cell.leftImageView.image = [UIImage imageNamed:@"gift"];
        cell.titleLabel.text = @"福利中心";
        cell.rightLabel.text = @"做任务,赢现金";
    }else if (indexPath.row == 1){
        cell.leftImageView.image = [UIImage imageNamed:@"invite"];
        cell.titleLabel.text = @"邀请好友";
        cell.rightLabel.text = @"领现金奖励,可提现";
    }else{
        cell.leftImageView.image = [UIImage imageNamed:@"dial"];
        cell.titleLabel.text = @"每日转红包";
        cell.rightLabel.text = @"领现金奖励,可提现";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        WelfareViewController *vc = [[WelfareViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
        
    }else{
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70*AdaptRatio;
}

@end
