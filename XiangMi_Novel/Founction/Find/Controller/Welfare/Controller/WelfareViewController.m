//
//  WelfareViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/17.
//  Copyright © 2019 zk. All rights reserved.
//

#import "WelfareViewController.h"
#import "WelfareTableViewCell.h"

@interface WelfareViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSArray *leftTitleArray;
@property (nonatomic,strong) NSArray *leftImageArray;
@property (nonatomic,strong) NSArray *leftMoneyArray;
@property (nonatomic,strong) NSArray *rightTitleArray;
@property (nonatomic,strong) NSArray *rightImageArray;
@property (nonatomic,strong) NSArray *rightMoneyArray;
@property (nonatomic,strong) UIImageView *lineView;
@property (nonatomic,strong) UIButton *leftBtn;
@property (nonatomic,strong) UIButton *rightBtn;
@property (nonatomic,assign) BOOL isLeft;

@end

@implementation WelfareViewController

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, AdaptNavigationHeight+40.6*AdaptRatio, DeviceWidth, DeviceHeight-AdaptNavigationHeight-SafeAreaBottomHeight-40.6*AdaptRatio) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
        
        [self.tableView registerClass:[WelfareTableViewCell1 class] forCellReuseIdentifier:@"WelfareTableViewCell1"];
        [self.tableView registerClass:[WelfareTableViewCell2 class] forCellReuseIdentifier:@"WelfareTableViewCell2"];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseUI:@"福利中心"];
    
    [self drawTopView];
    
    self.isLeft = YES;
    self.leftTitleArray = @[@"绑定微信",@"绑定手机号",@"绑定微博",@"首次加入书架",@"修改头像",@"修改昵称",@"首次开通会员"];
    self.leftImageArray = @[@"welfare_wechat",@"welfare_phone",@"welfare_weibo",@"welfare_book",@"welfare_head",@"welfare_name",@"welfare_vip"];
    self.leftMoneyArray = @[@"1",@"1",@"1",@"1",@"1",@"1",@"3"];
    self.rightTitleArray = @[@"连续七天签到",@"每日分享",@"每日阅读60分钟"];
    self.rightImageArray = @[@"welfare_signin",@"welfare_share",@"welfare_read60"];
    self.rightMoneyArray = @[@"3",@"0.5",@"0.5"];
    self.tableView.backgroundColor = WhiteColor;
}

- (void)request
{
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"post" url:@"" loadStr:@"" param:@{} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        [Tools showMessage:netStr];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isLeft == YES) {
        return 7;
    }else{
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isLeft == YES) {
        WelfareTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"WelfareTableViewCell1"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.rightBtn addTarget:self action:@selector(newTaskClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.rightBtn.tag = 400+indexPath.row;
        if (indexPath.row == 0) {
            [cell.rightBtn setTitle:@"可领取" forState:UIControlStateNormal];
            [cell.rightBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
            [cell.rightBtn setBackgroundImage:[UIImage imageNamed:@"welfare_uncompete_button"] forState:UIControlStateNormal];
        }
        
        cell.titleLabel.text = self.leftTitleArray[indexPath.row];
        cell.leftImageView.image = [UIImage imageNamed:self.leftImageArray[indexPath.row]];
        cell.moneyLabel.text = [NSString stringWithFormat:@"%@元",self.leftMoneyArray[indexPath.row]];
        return cell;
    }else{
        WelfareTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"WelfareTableViewCell2"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.rightBtn addTarget:self action:@selector(dayTaskClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.rightBtn.tag = 500+indexPath.row;
        
        cell.titleLabel.text = self.rightTitleArray[indexPath.row];
        cell.leftImageView.image = [UIImage imageNamed:self.rightImageArray[indexPath.row]];
        cell.moneyLabel.text = [NSString stringWithFormat:@"%@元",self.rightMoneyArray[indexPath.row]];
        return cell;
    }
}

//新手任务 根据tag值,判断执行事件
- (void)newTaskClick:(UIButton *)sender
{
    if (sender.tag - 400 == 0) {
        [sender setTitle:@"已领取" forState:UIControlStateNormal];
        sender.backgroundColor = ThemeColor;
        [sender setTitleColor:WhiteColor forState:UIControlStateNormal];
        sender.enabled = NO;
        [sender setBackgroundImage:[UIImage imageNamed:@"welfare_compete_button"] forState:UIControlStateNormal];
        sender.layer.borderWidth = 0;
    }else{
        [sender setTitle:@"已完成" forState:UIControlStateNormal];
        sender.backgroundColor = ThemeColor;
        [sender setTitleColor:WhiteColor forState:UIControlStateNormal];
        sender.enabled = NO;
        [sender setBackgroundImage:[UIImage imageNamed:@"welfare_compete_button"] forState:UIControlStateNormal];
        sender.layer.borderWidth = 0;
    }
}

//日常任务 根据tag值,判断执行事件
- (void)dayTaskClick:(UIButton *)sender
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70*AdaptRatio;
}


- (void)drawTopView
{
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = WhiteColor;
    [self.view addSubview:topView];
    topView.frame = CGRectMake(0, AdaptNavigationHeight, DeviceWidth, 40*AdaptRatio);
    
    self.lineView = [[UIImageView alloc] init];
    self.lineView.backgroundColor = ThemeColor;
    //self.lineView.image = [UIImage imageNamed:@"sure_bg_image"];
    self.lineView.frame = CGRectMake(DeviceWidth*1/4-30*AdaptRatio, 38*AdaptRatio, 60*AdaptRatio, 2*AdaptRatio);
    [topView addSubview:self.lineView];
    self.lineView.layer.masksToBounds = YES;
    self.lineView.layer.cornerRadius = 1.5;
    
    self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [topView addSubview:self.leftBtn];
    self.leftBtn.backgroundColor = ClearColor;
    [self.leftBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.leftBtn setTitle:@"新手任务" forState:UIControlStateNormal];
    [self.leftBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
    self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:17*AdaptRatio];
    self.leftBtn.frame = CGRectMake(0, 0, DeviceWidth*1/2, 39*AdaptRatio);
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [topView addSubview:self.rightBtn];
    self.rightBtn.backgroundColor = ClearColor;
    [self.rightBtn addTarget:self action:@selector(rightBtnnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.rightBtn setTitle:@"日常任务" forState:UIControlStateNormal];
    [self.rightBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:17*AdaptRatio];
    self.rightBtn.frame = CGRectMake(DeviceWidth*1/2, 0, DeviceWidth*1/2, 39*AdaptRatio);
    
    UIView *lineView = [[UIView alloc] init];
    lineView.frame = CGRectMake(0*AdaptRatio, AdaptNavigationHeight+40*AdaptRatio, DeviceWidth, 0.6*AdaptRatio);
    lineView.alpha = 0.3;
    lineView.backgroundColor = LightGrayColor;
    [self.view addSubview:lineView];
}

- (void)leftBtnClick:(UIButton *)sender
{
    [self.rightBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [self.leftBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.lineView.frame = CGRectMake(DeviceWidth*1/4-30*AdaptRatio, 38*AdaptRatio, 60*AdaptRatio,2*AdaptRatio);
    [UIView commitAnimations];
    self.isLeft = YES;
    
    [self.tableView reloadData];
}

- (void)rightBtnnClick:(UIButton *)sender
{
    [self.rightBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
    [self.leftBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.lineView.frame = CGRectMake(DeviceWidth*3/4-30*AdaptRatio, 38*AdaptRatio, 60*AdaptRatio, 2*AdaptRatio);
    [UIView commitAnimations];
    self.isLeft = NO;
    
    [self.tableView reloadData];
}

- (void)baseUI:(NSString *)titleStr
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = WhiteColor;
    self.title = titleStr;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self.view addSubview:backBtn];
    //backBtn.backgroundColor = RedColor;
    backBtn.frame = CGRectMake(0,0,25,25);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backBtnItem;
}
- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
