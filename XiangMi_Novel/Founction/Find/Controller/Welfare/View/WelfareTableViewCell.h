//
//  WelfareTableViewCell.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/17.
//  Copyright © 2019 zk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WelfareTableViewCell1 : UITableViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UIImageView *leftImageView;
@property (nonatomic,strong) UILabel *moneyLabel;
@property (nonatomic,strong) UIButton *rightBtn;
@end

@interface WelfareTableViewCell2 : UITableViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UIImageView *leftImageView;
@property (nonatomic,strong) UILabel *moneyLabel;
@property (nonatomic,strong) UIButton *rightBtn;
@end

NS_ASSUME_NONNULL_END
