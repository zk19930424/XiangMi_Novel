//
//  WelfareTableViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/17.
//  Copyright © 2019 zk. All rights reserved.
//

#import "WelfareTableViewCell.h"

@implementation WelfareTableViewCell1
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0*AdaptRatio, 0*AdaptRatio, DeviceWidth, 70*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15*AdaptRatio, 20*AdaptRatio, 30*AdaptRatio, 30*AdaptRatio)];
        //self.leftImageView.backgroundColor = RedColor;
        self.leftImageView.image = [UIImage imageNamed:@""];
        [self.baseView addSubview:self.leftImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65*AdaptRatio, 15*AdaptRatio, self.baseView.size.width-160*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = RedColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
        
        UILabel *Label = [[UILabel alloc] initWithFrame:CGRectMake(65*AdaptRatio, 35*AdaptRatio, 40*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:Label];
        //Label.backgroundColor = RedColor;
        Label.text = @"奖励: ";
        Label.textAlignment = NSTextAlignmentLeft;
        Label.textColor = LightGrayColor;
        Label.font = [UIFont systemFontOfSize:14*AdaptRatio];
        
        self.moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(105*AdaptRatio, 35*AdaptRatio, 100*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.moneyLabel];
        //self.moneyLabel.backgroundColor = GreenColor;
        self.moneyLabel.text = @"";
        self.moneyLabel.textAlignment = NSTextAlignmentLeft;
        self.moneyLabel.textColor = ThemeColor;
        self.moneyLabel.font = [UIFont systemFontOfSize:14*AdaptRatio];
        
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //rightBtn.backgroundColor = [UIColor redColor];
        self.rightBtn.frame = CGRectMake(self.baseView.size.width-85*AdaptRatio,20*AdaptRatio,70*AdaptRatio,30*AdaptRatio);
        self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:14*AdaptRatio];
        [self.rightBtn setTitle:@"去完成" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
        [self.baseView addSubview:self.rightBtn];
        self.rightBtn.layer.borderColor = ThemeColor.CGColor;
        self.rightBtn.layer.borderWidth = 1*AdaptRatio;
        self.rightBtn.layer.masksToBounds = YES;
        self.rightBtn.layer.cornerRadius = 30*AdaptRatio/2;
        
        UIView *lineView = [[UIView alloc] init];
        lineView.frame = CGRectMake(0*AdaptRatio, 69.4*AdaptRatio, DeviceWidth, 0.6*AdaptRatio);
        lineView.alpha = 0.3;
        lineView.backgroundColor = LightGrayColor;
        [self.baseView addSubview:lineView];
    }
    return self;
}
@end


@implementation WelfareTableViewCell2
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0*AdaptRatio, 0*AdaptRatio, DeviceWidth, 70*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15*AdaptRatio, 20*AdaptRatio, 30*AdaptRatio, 30*AdaptRatio)];
        //self.leftImageView.backgroundColor = RedColor;
        self.leftImageView.image = [UIImage imageNamed:@""];
        [self.baseView addSubview:self.leftImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65*AdaptRatio, 15*AdaptRatio, self.baseView.size.width-160*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = RedColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
        
        UILabel *Label = [[UILabel alloc] initWithFrame:CGRectMake(65*AdaptRatio, 35*AdaptRatio, 40*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:Label];
        //Label.backgroundColor = RedColor;
        Label.text = @"奖励: ";
        Label.textAlignment = NSTextAlignmentLeft;
        Label.textColor = LightGrayColor;
        Label.font = [UIFont systemFontOfSize:14*AdaptRatio];
        
        self.moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(105*AdaptRatio, 35*AdaptRatio, 100*AdaptRatio, 20*AdaptRatio)];
        [self.baseView addSubview:self.moneyLabel];
        //self.moneyLabel.backgroundColor = GreenColor;
        self.moneyLabel.text = @"";
        self.moneyLabel.textAlignment = NSTextAlignmentLeft;
        self.moneyLabel.textColor = ThemeColor;
        self.moneyLabel.font = [UIFont systemFontOfSize:14*AdaptRatio];
        
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //rightBtn.backgroundColor = [UIColor redColor];
        self.rightBtn.frame = CGRectMake(self.baseView.size.width-85*AdaptRatio,20*AdaptRatio,70*AdaptRatio,30*AdaptRatio);
        self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:14*AdaptRatio];
        [self.rightBtn setTitle:@"去完成" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
        [self.baseView addSubview:self.rightBtn];
        self.rightBtn.layer.borderColor = ThemeColor.CGColor;
        self.rightBtn.layer.borderWidth = 1*AdaptRatio;
        self.rightBtn.layer.masksToBounds = YES;
        self.rightBtn.layer.cornerRadius = 30*AdaptRatio/2;
        
        UIView *lineView = [[UIView alloc] init];
        lineView.frame = CGRectMake(0*AdaptRatio, 69.4*AdaptRatio, DeviceWidth, 0.6*AdaptRatio);
        lineView.alpha = 0.3;
        lineView.backgroundColor = LightGrayColor;
        [self.baseView addSubview:lineView];
    }
    return self;
}
@end

