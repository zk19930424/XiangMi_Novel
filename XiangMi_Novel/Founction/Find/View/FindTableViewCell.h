//
//  FindTableViewCell.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/17.
//  Copyright © 2019 zk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FindTableViewCell : UITableViewCell
@property (nonatomic,strong) UIView *baseView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UIImageView *leftImageView;
@property (nonatomic,strong) UILabel *rightLabel;
@end

NS_ASSUME_NONNULL_END
