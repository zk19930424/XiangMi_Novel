//
//  FindTableViewCell.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/17.
//  Copyright © 2019 zk. All rights reserved.
//

#import "FindTableViewCell.h"

@implementation FindTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.baseView = [[UIView alloc] init];
        self.baseView.frame = CGRectMake(0*AdaptRatio, 0*AdaptRatio, DeviceWidth, 65*AdaptRatio);
        self.baseView.backgroundColor = WhiteColor;
        [self.contentView addSubview:self.baseView];
        
        self.leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15*AdaptRatio, 17.5*AdaptRatio, 30*AdaptRatio, 30*AdaptRatio)];
        //self.leftImageView.backgroundColor = RedColor;
        self.leftImageView.image = [UIImage imageNamed:@""];
        [self.baseView addSubview:self.leftImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(55*AdaptRatio, 20*AdaptRatio, 100*AdaptRatio, 25*AdaptRatio)];
        [self.baseView addSubview:self.titleLabel];
        //self.titleLabel.backgroundColor = RedColor;
        self.titleLabel.text = @"";
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
        
        self.rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(155*AdaptRatio, 20*AdaptRatio, self.baseView.size.width-200*AdaptRatio, 25*AdaptRatio)];
        [self.baseView addSubview:self.rightLabel];
        //self.rightLabel.backgroundColor = RedColor;
        self.rightLabel.text = @"";
        self.rightLabel.textAlignment = NSTextAlignmentRight;
        self.rightLabel.textColor = LightGrayColor;
        self.rightLabel.font = [UIFont systemFontOfSize:13*AdaptRatio];
    }
    return self;
}
@end
