//
//  BindingPhoneViewController.h
//  BlackDoPlanet
//
//  Created by Botao on 2018/10/15.
//  Copyright © 2018年 botaoweb. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BindingPhoneViewController : UIViewController

@property (nonatomic,strong) NSMutableDictionary *thirdInfoMutDic;

@end

NS_ASSUME_NONNULL_END
