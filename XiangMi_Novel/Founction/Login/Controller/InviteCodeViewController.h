//
//  InviteCodeViewController.h
//  BlackDoPlanet
//
//  Created by 冯业宁 on 2018/8/7.
//  Copyright © 2018年 botaoweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteCodeViewController : UIViewController
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *phone;

@end
