//
//  InviteCodeViewController.m
//  BlackDoPlanet
//
//  Created by 冯业宁 on 2018/8/7.
//  Copyright © 2018年 botaoweb. All rights reserved.
//

#import "InviteCodeViewController.h"
#import "TabBarViewController.h"
@interface InviteCodeViewController ()<UITextViewDelegate, UIGestureRecognizerDelegate>
@property(nonatomic, assign)float xWidth;
@property(nonatomic, strong)UIImageView *BGImgeView;
@property(nonatomic, strong)UITextView *inviteCodeTV;
@property(nonatomic, strong)UIButton *inviteCodeBtn;
@property(nonatomic, strong)NSString *inviteCodeStr;
@property(nonatomic, strong)UIButton *submitBtn;
@property(nonatomic, assign)BOOL isInputInviteCode;


@end

@implementation InviteCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _xWidth = [Tools theSize:@"width"];
    _inviteCodeStr = @"";
    _isInputInviteCode = NO;
    
    [self initTopView];
    [self setGesture];
}

- (void)initTopView {
    _BGImgeView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    _BGImgeView.backgroundColor = WhiteColor;
    _BGImgeView.frame = self.view.frame;
    _BGImgeView.userInteractionEnabled = YES;
    [self.view addSubview:_BGImgeView];
    
    UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(35*_xWidth, [Tools getStatusBarHeight]+64*_xWidth, self.view.frame.size.width-35*2*_xWidth, 40*_xWidth)];
    [infoLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:20*[Tools theSize:@"width"]]];
    infoLabel.text = @"填写邀请码，+888黑钻";
    infoLabel.textColor = BlackColor;
    infoLabel.textAlignment = NSTextAlignmentCenter;
    [_BGImgeView addSubview:infoLabel];
    
    _inviteCodeBtn = [[UIButton alloc]initWithFrame:CGRectMake(35*_xWidth, infoLabel.frame.origin.y+infoLabel.frame.size.height+60*_xWidth, self.view.frame.size.width-35*2*_xWidth, 30*_xWidth)];
    [_inviteCodeBtn setTitle:@"输入邀请码（选填）" forState:UIControlStateNormal];
    [_inviteCodeBtn setTitleColor:UIColorFromRGB(0xccccd2) forState:UIControlStateNormal];
    [_inviteCodeBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    _inviteCodeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_inviteCodeBtn addTarget:self action:@selector(inputInviteCodeAction) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:_inviteCodeBtn];
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(_inviteCodeBtn.frame.origin.x, _inviteCodeBtn.frame.origin.y+_inviteCodeBtn.frame.size.height+1, _inviteCodeBtn.frame.size.width, 1)];
    [line1 setBackgroundColor:UIColorFromRGB(0xccccd2)];
     line1.alpha = 0.5;
    [_BGImgeView addSubview:line1];
    
    _submitBtn = [[UIButton alloc]initWithFrame:CGRectMake(35*_xWidth, line1.frame.origin.y+36*_xWidth, self.view.frame.size.width-35*2*_xWidth, 39*_xWidth)];
    [_submitBtn setBackgroundImage:[UIImage imageNamed:@"button_rectangle"] forState:UIControlStateNormal];
    [_submitBtn setTitleColor:[Tools hexStringToUIColor:@"fdf5ff"] forState:UIControlStateNormal];
    [_submitBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:17*[Tools theSize:@"width"]]];
    [_submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitInviteCode) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:_submitBtn];
    
    UIButton *skipBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-65*_xWidth, [Tools getStatusBarHeight], 30*_xWidth, 44*_xWidth)];
    [skipBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [skipBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    [skipBtn setTitle:@"跳过" forState:UIControlStateNormal];
    [skipBtn addTarget:self action:@selector(skipSubmitInviteCode) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:skipBtn];
}

- (void)inputInviteCodeAction {
    _inviteCodeTV = [[UITextView alloc]initWithFrame:_inviteCodeBtn.frame];
    [_inviteCodeTV setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    [_inviteCodeTV setBackgroundColor:[UIColor whiteColor]];
    _inviteCodeTV.textColor = [UIColor blackColor];
    _inviteCodeTV.delegate = self;
    _inviteCodeTV.keyboardType = UIKeyboardTypeASCIICapable;
    [_inviteCodeTV becomeFirstResponder];
    [_BGImgeView addSubview:_inviteCodeTV];
}

- (void)skipSubmitInviteCode {
    [self gotoMainVC];
}

- (void)submitInviteCode {
    [Tools showMessage:@"提交中..."];
    NSDictionary *params = @{@"uid":[Dao findUserInfo].userId,
                             @"phone":[Dao findUserInfo].telephone,
                             @"invitcode":_inviteCodeStr
                             };
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"post" url:[RequestUrl theInvitCodeUrlHost] loadStr:@"" param:params Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            [self gotoMainVC];
        }else {
            self.inviteCodeStr = @"";
            [Tools showMessage:@"邀请码有误或已失效"];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        
    }];
  
}

- (void)gotoMainVC {
    [self dismissViewControllerAnimated:YES completion:nil];
    self.view.window.rootViewController = [[TabBarViewController alloc] init];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ((text.length == 0)&&(textView.text.length>0)) {
        _inviteCodeStr = [NSString stringWithFormat:@"%@%@", [textView.text substringToIndex:textView.text.length-1], text];
    }else {
        _inviteCodeStr = [NSString stringWithFormat:@"%@%@", textView.text, text];
    }
    if ([@"" isEqualToString:_inviteCodeStr]) {
        _isInputInviteCode = NO;
    }else {
        _isInputInviteCode = YES;
    }
    return YES;
}

#pragma mark 设置手势
-(void)setGesture{
    UITapGestureRecognizer *recognierUp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignFirstResponder:)];
    [recognierUp setDelegate:self];
    [_BGImgeView addGestureRecognizer:recognierUp];
}
-(IBAction)resignFirstResponder:(id)sender{
    if (!_isInputInviteCode) {
        [_inviteCodeTV setHidden:YES];
    }
    [_inviteCodeTV resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
