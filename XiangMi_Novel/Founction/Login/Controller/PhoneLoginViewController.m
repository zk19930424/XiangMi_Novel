//
//  PhoneLoginViewController.m
//  BlackDoPlanet
//
//  Created by Botao on 2018/9/20.
//  Copyright © 2018年 botaoweb. All rights reserved.
//

#import "PhoneLoginViewController.h"
#import "InviteCodeViewController.h"
#import "PrivacyProtocolViewController.h"
#import "TabBarViewController.h"
//#import "MPPanViewController.h"
#import "BindingPhoneViewController.h"
#import <UMShare/UMShare.h>

@interface PhoneLoginViewController ()<UITextViewDelegate, UIGestureRecognizerDelegate>
{
    NSString *_commonStr;
}
@property(nonatomic, assign)float xWidth;
@property(nonatomic, strong)UIImageView *BGImgeView;
@property(nonatomic, strong)UITextView *phoneNumTV;
@property(nonatomic, strong)UITextView *verifyTV;
@property(nonatomic, strong)UIButton *phoneBtn;
@property(nonatomic, strong)UIButton *verifyBtn;
@property(nonatomic, strong)UIButton *getVerifyBtn;
@property(nonatomic, strong)UIButton *submitBtn;
@property(nonatomic, strong)NSString *phoneNumStr;
@property(nonatomic, strong)NSString *verifyStr;
@property(nonatomic, assign)BOOL isInputPhoneNum;
@property(nonatomic, assign)BOOL isGetVerify;
@property(nonatomic, assign)BOOL isInputVerify;
@property(nonatomic, assign)int timeCount;

@property(nonatomic, strong)NSString *deviceId;

@end

@implementation PhoneLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user removeObjectForKey:@"isSaveUUID"];
    [user setBool:NO forKey:@"sureLogin"];
    //登录状态为0
    [Dao updateIsLogin:@"0"];
    
    self.xWidth = [Tools theSize:@"width"];
    
    _commonStr = @"";
    self.phoneNumStr = @"";
    self.verifyStr = @"";
    _isInputPhoneNum = NO;
    _isGetVerify = NO;
    _isInputVerify = NO;
    _timeCount = 60;
    _deviceId = [SAMKeychain passwordForService:@"com.yourcompany.yourapp" account:@"user"];
    
    [self initTopView];
    [self initSkipAction];
    
    if ([user boolForKey:@"isIosFlag=1"] == YES) {
        
    }else{
        [self initBottomView];
    }
}

- (void)initBottomView {
    
    UIButton *loginQQ = [[UIButton alloc]initWithFrame:CGRectMake(40*_xWidth, self.view.frame.size.height-(28+(DeviceWidth-160*_xWidth)/3)*_xWidth-SafeAreaBottomHeight, (DeviceWidth-160*_xWidth)/3, (DeviceWidth-160*_xWidth)/3)];
    [loginQQ setBackgroundImage:[UIImage imageNamed:@"icon_qq"] forState:UIControlStateNormal];
    [loginQQ addTarget:self action:@selector(loginQQ) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:loginQQ];
    
    UIButton *loginwechat = [[UIButton alloc]initWithFrame:CGRectMake((DeviceWidth-160*_xWidth)/3+80*_xWidth, loginQQ.frame.origin.y, (DeviceWidth-160*_xWidth)/3, (DeviceWidth-160*_xWidth)/3)];
    [loginwechat setBackgroundImage:[UIImage imageNamed:@"icon_wechat"] forState:UIControlStateNormal];
    [loginwechat addTarget:self action:@selector(loginwechat) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:loginwechat];
    
    UIButton *loginweb = [[UIButton alloc]initWithFrame:CGRectMake((DeviceWidth-160*_xWidth)/3*2+120*_xWidth, loginQQ.frame.origin.y, (DeviceWidth-160*_xWidth)/3, (DeviceWidth-160*_xWidth)/3)];
    [loginweb setBackgroundImage:[UIImage imageNamed:@"icon_web"] forState:UIControlStateNormal];
    [loginweb addTarget:self action:@selector(loginweb) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:loginweb];
     
    
    /*
    UIButton *loginQQ = [[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth*2/4-(DeviceWidth-160*_xWidth)/3-30*AdaptRatio, self.view.frame.size.height-(28+(DeviceWidth-160*_xWidth)/3)*_xWidth-SafeAreaBottomHeight, (DeviceWidth-160*_xWidth)/3, (DeviceWidth-160*_xWidth)/3)];
    [loginQQ setBackgroundImage:[UIImage imageNamed:@"icon_qq"] forState:UIControlStateNormal];
    [loginQQ addTarget:self action:@selector(loginQQ) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:loginQQ];
    
    UIButton *loginwechat = [[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth*2/4+30*AdaptRatio, loginQQ.frame.origin.y, (DeviceWidth-160*_xWidth)/3, (DeviceWidth-160*_xWidth)/3)];
    [loginwechat setBackgroundImage:[UIImage imageNamed:@"icon_wechat"] forState:UIControlStateNormal];
    [loginwechat addTarget:self action:@selector(loginwechat) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:loginwechat];
    */
    
    
    UILabel *loginLabel = [[UILabel alloc]initWithFrame:CGRectMake(35*_xWidth, loginQQ.frame.origin.y-(28+15)*_xWidth, self.view.frame.size.width-35*2*_xWidth, 15*_xWidth)];
    loginLabel.text = @"快捷登陆";
    loginLabel.textColor = BlackColor;
    loginLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*_xWidth];
    loginLabel.textAlignment = NSTextAlignmentCenter;
    [_BGImgeView addSubview:loginLabel];
}

- (void)loginwechat {
    _commonStr = @"113";
    [self getUserInfoForPlatform:UMSocialPlatformType_WechatSession type:@"2"];
}
- (void)loginQQ {
    _commonStr = @"112";
    [self getUserInfoForPlatform:UMSocialPlatformType_QQ type:@"1"];
}
- (void)loginweb {
    _commonStr = @"114";
    [self getUserInfoForPlatform:UMSocialPlatformType_Sina type:@"3"];
}
- (void)getUserInfoForPlatform:(UMSocialPlatformType)platformType type:(NSString *)type
{
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:platformType currentViewController:nil completion:^(id result, NSError *error)
    {
        UMSocialUserInfoResponse *resp = result;
        // 第三方登录数据(为空表示平台未提供)
        // 授权数据
        NSLog(@" uid: %@", resp.uid);
        NSLog(@" openid: %@", resp.openid);
        NSLog(@" accessToken: %@", resp.accessToken);
        NSLog(@" refreshToken: %@", resp.refreshToken);
        NSLog(@" expiration: %@", resp.expiration);
        // 用户数据
        NSLog(@" name: %@", resp.name);
        NSLog(@" iconurl: %@", resp.iconurl);
        NSLog(@" gender: %@ --- unionId = %@", resp.unionGender,resp.unionId);
        // 第三方平台SDK原始数据
        NSLog(@" originalResponse: %@", resp.originalResponse);
        if (resp) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
            [dict setObject:resp.name forKey:@"name"];
            [dict setObject:resp.iconurl forKey:@"iconurl"];
            [dict setObject:resp.uid forKey:@"uid"];
            [dict setObject:resp.usid forKey:@"openid"];
            [dict setObject:resp.unionId?:@"" forKey:@"unionId"];
            [dict setObject:resp.unionGender forKey:@"unionGender"];
            [dict setObject:resp.gender forKey:@"gender"];
            [dict setObject:type forKey:@"type"];
            [dict setObject:resp.accessToken forKey:@"accessToken"];
            
            // 三方登陆授权成功请求后台反馈接口，后台返回是否已经绑定手机号
            NSDictionary *params = @{@"tid":dict[@"uid"],
                                     @"phone":@"",
                                     @"code":@"",
                                     @"type":dict[@"type"],
                                     @"sid":self->_deviceId,
                                     @"uid":@""
                                     };

            [ZK_HUD showUIBlockingIndicatorWithText:@""];
            [[HttpManager shareInstance] loginRequestWithUrl:[RequestUrl TheLogin_post] param:params token:[Dao findUserInfo].token?:@"" Complete:^(NSInteger code, NSDictionary *dictionary)
            {
                [ZK_HUD hideUIBlockingIndicator];
                NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                if ([self->_commonStr integerValue] == code) {
                    // 如果 未绑定 进入绑定界面，将三方授权的信息用Dict的方式带上和即将绑定的手机号一起提交
                    NSMutableDictionary *thirdInfoMutDic = [NSMutableDictionary dictionary];
                    [thirdInfoMutDic setObject:dict[@"accessToken"] forKey:@"accessToken"];
                    [thirdInfoMutDic setObject:dict[@"openid"] forKey:@"code"];
                    [thirdInfoMutDic setObject:dict[@"expires_in"]?:@"" forKey:@"expiresIn"];
                    [thirdInfoMutDic setObject:dict[@"gender"] forKey:@"gender"];
                    [thirdInfoMutDic setObject:dict[@"iconurl"] forKey:@"iconurl"];
                    [thirdInfoMutDic setObject:dict[@"name"] forKey:@"name"];
                    [thirdInfoMutDic setObject:dict[@"openid"] forKey:@"openid"];
                    [thirdInfoMutDic setObject:@"123456" forKey:@"phone"];
                    [thirdInfoMutDic setObject:@"123456" forKey:@"province"];
                    [thirdInfoMutDic setObject:dict[@"uid"]?:@"" forKey:@"tid"];
                    [thirdInfoMutDic setObject:dict[@"type"] forKey:@"type"];
                    [thirdInfoMutDic setObject:dict[@"unionid"]?:@"" forKey:@"unionid"];
                    
                    BindingPhoneViewController *vc = [[BindingPhoneViewController alloc] init];
                    vc.thirdInfoMutDic = [NSMutableDictionary dictionary];
                    vc.thirdInfoMutDic = thirdInfoMutDic;
                    [self presentViewController:vc animated:YES completion:nil];
                }else if (code == 0) {
                    [user setBool:YES forKey:@"sureLogin"];
                    NSMutableDictionary *tempMuDict = [NSMutableDictionary dictionaryWithDictionary:[dictionary objectForKey:@"result"]];
                    [tempMuDict setValue:dictionary[@"token"] forKey:@"token"];
                    //存用户数据
                    [self userInfoCoreData:tempMuDict];
                }else {
                    [user setBool:NO forKey:@"sureLogin"];
                    [Tools showMessage:[dictionary objectForKey:@"msg"]];
                }
            }];
            
        }else{
            [Tools showMessage:@"正在接入中"];
        }
    }];
}


//立即开启
- (void)submitPhone {
    [_phoneNumTV resignFirstResponder];
    [_verifyTV resignFirstResponder];
    
    if (_phoneNumStr.length != 11) {
        [Tools showMessage:@"手机号不符合要求"];
        return;
    }
    NSDictionary *params = @{@"qvwid":@"",
                             @"telephone":_phoneNumStr,
                             @"code":_verifyStr,
                             @"type":@"0",
                             @"mimeid":_deviceId?:@""
                             };
    [[HttpManager shareInstance] loginRequestWithUrl:[RequestUrl TheLogin_post] param:params token:[Dao findUserInfo].token?:@"" Complete:^(NSInteger code, NSDictionary *dictionary)
    {
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        if (code == 0) {
            [user setBool:YES forKey:@"sureLogin"];
            NSMutableDictionary *tempMuDict = [NSMutableDictionary dictionaryWithDictionary:[dictionary objectForKey:@"result"]];
            [tempMuDict setValue:dictionary[@"token"] forKey:@"token"];
            //存用户数据
            [self userInfoCoreData:tempMuDict];
        }else{
            [user setBool:NO forKey:@"sureLogin"];
            [Tools showMessage:[dictionary objectForKey:@"msg"]];
        }
        
    }];
}

//存用户数据
- (void)userInfoCoreData:(NSDictionary *)dic {
    NSString *balance = [NSString stringWithFormat:@"%@",[dic objectForKey:@"balance"]];
    NSString *firstLogin = [NSString stringWithFormat:@"%@",[dic objectForKey:@"firstLogin"]];
    NSString *icon = [NSString stringWithFormat:@"%@",[dic objectForKey:@"icon"]];
    NSString *invitCode = [NSString stringWithFormat:@"%@",[dic objectForKey:@"invitCode"]];
    
    NSString *invitCodeFrom = [NSString stringWithFormat:@"%@",[dic objectForKey:@"invitCodeFrom"]];
    NSString *level = [NSString stringWithFormat:@"%@",[dic objectForKey:@"level"]];
    NSString *nickName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"nickName"]];
    
    NSString *openId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"openId"]];
    NSString *password = [NSString stringWithFormat:@"%@",[dic objectForKey:@"password"]];
    NSString *qqid = [NSString stringWithFormat:@"%@",[dic objectForKey:@"qqid"]];
    NSString *source = [NSString stringWithFormat:@"%@",[dic objectForKey:@"source"]];
    
    NSString *telephone = [NSString stringWithFormat:@"%@",[dic objectForKey:@"telephone"]];
    NSString *userId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"userId"]];
    NSString *userName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"userName"]];
    NSString *valid = [NSString stringWithFormat:@"%@",[dic objectForKey:@"valid"]];
    NSString *vxid = [NSString stringWithFormat:@"%@",[dic objectForKey:@"vxid"]];
    NSString *wbid = [NSString stringWithFormat:@"%@",[dic objectForKey:@"wbid"]];
    NSString *token = [NSString stringWithFormat:@"%@",[dic objectForKey:@"token"]];

    NSDictionary *userInfoDic = [NSDictionary dictionaryWithObjectsAndKeys:
                                 balance, @"balance",
                                 firstLogin, @"firstLogin",
                                 icon, @"icon",
                                 invitCode, @"invitCode",
                                 invitCodeFrom, @"invitCodeFrom",
                                 level, @"level",
                                 nickName, @"nickName",
                                 openId, @"openId",
                                 password, @"password",
                                 qqid, @"qqid",
                                 source, @"source",
                                 telephone, @"telephone",
                                 userId,@"userId",
                                 userName,@"userName",
                                 valid,@"valid",
                                 vxid,@"vxid",
                                 wbid,@"wbid",
                                 @"1",@"isLogin",
                                 token,@"token",
                                 nil];
    UserModel *userModel = [[UserModel alloc]initWithDicForUserInfo:userInfoDic];
    [Dao login:userModel];
    
    if ([@"1" isEqualToString:source]) {
        // 邀请码VC
        InviteCodeViewController *inviteCodeVC = [[InviteCodeViewController alloc]init];
        [self presentViewController:inviteCodeVC animated:YES completion:nil];
    }else {
        [self dismissViewControllerAnimated:YES completion:nil];
        self.view.window.rootViewController = [[TabBarViewController alloc] init];
    }
}


#pragma mark TextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (textView == _phoneNumTV) {
        if ((text.length == 0)&&(textView.text.length>0)) {
            _phoneNumStr = [NSString stringWithFormat:@"%@%@", [textView.text substringToIndex:textView.text.length-1], text];
        }else {
            _phoneNumStr = [NSString stringWithFormat:@"%@%@", textView.text, text];
        }
        if ([@"" isEqualToString:_phoneNumStr]) {
            _isInputPhoneNum = NO;
        }else {
            _isInputPhoneNum = YES;
        }
        
    }else if (textView == _verifyTV) {
        if ((text.length == 0)&&(textView.text.length>0)) {
            _verifyStr = [NSString stringWithFormat:@"%@%@", [textView.text substringToIndex:textView.text.length-1], text];
        }else {
            _verifyStr = [NSString stringWithFormat:@"%@%@", textView.text, text];
        }
        if ([@"" isEqualToString:_verifyStr]) {
            _isInputVerify = NO;
            [_submitBtn setBackgroundImage:[UIImage imageNamed:@"button_rectangle"] forState:UIControlStateNormal];
            _submitBtn.userInteractionEnabled = NO;
        }else {
            _isInputVerify = YES;
            [_submitBtn setBackgroundImage:[UIImage imageNamed:@"button_rectangle"] forState:UIControlStateNormal];
            _submitBtn.userInteractionEnabled = YES;
        }
    }
    return YES;
}



- (void)initSkipAction {
    UIButton *skipBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-65*_xWidth, [Tools getStatusBarHeight], 30*_xWidth, 44*_xWidth)];
    [skipBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [skipBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    [skipBtn setTitle:@"跳过" forState:UIControlStateNormal];
    [skipBtn addTarget:self action:@selector(skipLogin) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:skipBtn];
}

- (void)skipLogin {
    //[self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginOutSkip" object:nil];
    //self.view.window.rootViewController = [[TabBarViewController alloc] init];
//    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:[InitTabBar setTabBarSelectedVC:@"mainVC"]];
//    [nav setNavigationBarHidden:YES];
//    [UIApplication sharedApplication].windows.firstObject.rootViewController = [[TabBarViewController alloc] init];
//    [[UIApplication sharedApplication].windows.firstObject makeKeyAndVisible];
}

#pragma mark 设置手势
-(void)setGesture{
    UITapGestureRecognizer *recognierUp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignFirstResponder:)];
    [recognierUp setDelegate:self];
    [_BGImgeView addGestureRecognizer:recognierUp];
}
-(IBAction)resignFirstResponder:(id)sender{
    NSLog(@"_phoneNumStr: %@", _phoneNumStr);
    NSLog(@"_verifyStr: %@", _verifyStr);
    if (!_isInputPhoneNum) {
        [_phoneNumTV setHidden:YES];
    }
    if (!_isInputVerify) {
        [_verifyTV setHidden:YES];
    }
    [_phoneNumTV resignFirstResponder];
    [_verifyTV resignFirstResponder];
}



- (void)initTopView {
    
//    UIImage *image = [UIImage imageNamed:@"login_background"];
//    _BGImgeView = [[UIImageView alloc]initWithImage:image];
//    NSLog(@"%.2f --- %.2f",DeviceWidth,DeviceHeight);
//    _BGImgeView.frame = CGRectMake(0, 0, DeviceWidth, DeviceWidth*image.size.height/image.size.width);
//    _BGImgeView.userInteractionEnabled = YES;
//    [self.view addSubview:_BGImgeView];
    
    _BGImgeView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];//login_background
    _BGImgeView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    _BGImgeView.userInteractionEnabled = YES;
    _BGImgeView.backgroundColor = WhiteColor;
    [self.view addSubview:_BGImgeView];
    
    UIImageView *planet = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"login_logo"]];
    planet.frame = CGRectMake(self.view.frame.size.width/2-140/2*_xWidth, [Tools getStatusBarHeight]+50*_xWidth, 140*_xWidth, 140*_xWidth);
    [_BGImgeView addSubview:planet];
    
    _phoneBtn = [[UIButton alloc]initWithFrame:CGRectMake(35*_xWidth, planet.frame.origin.y+planet.frame.size.height+20*_xWidth, self.view.frame.size.width-35*2*_xWidth, 30*_xWidth)];
    [_phoneBtn setTitle:@"输入手机号" forState:UIControlStateNormal];
    [_phoneBtn setTitleColor:UIColorFromRGB(0xccccd2) forState:UIControlStateNormal];
    [_phoneBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    _phoneBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_phoneBtn addTarget:self action:@selector(inputPhoneNumAction) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:_phoneBtn];
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(_phoneBtn.frame.origin.x, _phoneBtn.frame.origin.y+_phoneBtn.frame.size.height+1, _phoneBtn.frame.size.width, 1)];
    [line1 setBackgroundColor:UIColorFromRGB(0xccccd2)];
    [_BGImgeView addSubview:line1];
    line1.alpha = 0.5;
    
    _getVerifyBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-(35+15*6)*_xWidth, line1.frame.origin.y+line1.frame.size.height+30*_xWidth, 15*6*_xWidth, 30*_xWidth)];
    [_getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_getVerifyBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
    [_getVerifyBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*_xWidth]];
    _getVerifyBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [_getVerifyBtn addTarget:self action:@selector(verifyAction) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:_getVerifyBtn];
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(_getVerifyBtn.frame.origin.x-1, _getVerifyBtn.frame.origin.y+7*_xWidth, 1, 16*_xWidth)];
    [line2 setBackgroundColor:UIColorFromRGB(0xccccd2)];
    [_BGImgeView addSubview:line2];
    line2.alpha = 0.5;
    
    _verifyBtn = [[UIButton alloc]initWithFrame:CGRectMake(35*_xWidth, _getVerifyBtn.frame.origin.y, line2.frame.origin.x-35*_xWidth, 30*_xWidth)];
    [_verifyBtn setTitle:@"请输入短信验证码" forState:UIControlStateNormal];
    [_verifyBtn setTitleColor:UIColorFromRGB(0xccccd2) forState:UIControlStateNormal];
    [_verifyBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    _verifyBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_verifyBtn addTarget:self action:@selector(inputVerifyAction) forControlEvents:UIControlEventTouchUpInside];
    [_BGImgeView addSubview:_verifyBtn];
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(line1.frame.origin.x, _verifyBtn.frame.origin.y+_verifyBtn.frame.size.height+1, line1.frame.size.width, 1)];
    line3.alpha = 0.5;
    [line3 setBackgroundColor:UIColorFromRGB(0xccccd2)];
    [_BGImgeView addSubview:line3];
    
    _submitBtn = [[UIButton alloc]initWithFrame:CGRectMake(35*_xWidth, line3.frame.origin.y+36*_xWidth, self.view.frame.size.width-35*2*_xWidth, 39*_xWidth)];
    //_submitBtn.backgroundColor = ThemeColor;
    [_submitBtn setBackgroundImage:[UIImage imageNamed:@"button_rectangle"] forState:UIControlStateNormal];
    [_submitBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [_submitBtn.titleLabel setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:17*[Tools theSize:@"width"]]];
    [_submitBtn addTarget:self action:@selector(submitPhone) forControlEvents:UIControlEventTouchUpInside];
    _submitBtn.userInteractionEnabled = NO;
    [_BGImgeView addSubview:_submitBtn];
    [self.submitBtn setTitle:@"立即开启" forState:UIControlStateNormal];
    _submitBtn.layer.masksToBounds = YES;
    _submitBtn.layer.cornerRadius = 39*_xWidth/2;
    
    UIView *protocolView = [[UIView alloc] initWithFrame:CGRectMake(35*_xWidth, line3.frame.origin.y+(36+10)*_xWidth+39*_xWidth, self.view.frame.size.width-35*2*_xWidth, 39*_xWidth)];
    protocolView.backgroundColor = [UIColor clearColor];
    [_BGImgeView addSubview:protocolView];
    
    UILabel *leftLabel = [[UILabel alloc] init];
    leftLabel.frame = CGRectMake(0, 0, (self.view.frame.size.width-35*2*_xWidth)/2, 39*_xWidth);
    leftLabel.backgroundColor = [UIColor clearColor];
    leftLabel.textAlignment = NSTextAlignmentRight;
    leftLabel.text = @"开启即表示同意";
    leftLabel.textColor = [UIColor lightGrayColor];
    leftLabel.font = [UIFont systemFontOfSize:13*_xWidth];
    [protocolView addSubview:leftLabel];
    
    UILabel *rightLabel = [[UILabel alloc] init];
    rightLabel.frame = CGRectMake((self.view.frame.size.width-35*2*_xWidth)/2, 0, (self.view.frame.size.width-35*2*_xWidth)/2, 39*_xWidth);
    rightLabel.backgroundColor = [UIColor clearColor];
    rightLabel.textAlignment = NSTextAlignmentLeft;
    
    NSMutableAttributedString *attributedString1 = [[NSMutableAttributedString alloc] initWithString:@"《用户服务协议》"];
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@1};
    [attributedString1 setAttributes:dict1 range:NSMakeRange(1, 6)];
    rightLabel.attributedText = attributedString1;
    
    //[rightLabel sizeToFit];
    rightLabel.textColor = [UIColor lightGrayColor];
    rightLabel.font = [UIFont systemFontOfSize:13*_xWidth];
    rightLabel.userInteractionEnabled = YES;
    [protocolView addSubview:rightLabel];
    
    UITapGestureRecognizer *tapGeature = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGeature)];
    [rightLabel addGestureRecognizer:tapGeature];
    
    [self setGesture];
}

- (void)tapGeature
{
    PrivacyProtocolViewController *vc = [[PrivacyProtocolViewController alloc] init];
    [self presentViewController:vc animated:YES completion:nil];
    //[self.navigationController pushViewController:vc animated:YES];
}

- (void)inputPhoneNumAction {
    _phoneNumTV = [[UITextView alloc]initWithFrame:_phoneBtn.frame];
    [_phoneNumTV setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    [_phoneNumTV setBackgroundColor:WhiteColor];
    _phoneNumTV.textColor = [UIColor blackColor];
    _phoneNumTV.delegate = self;
    _phoneNumTV.keyboardType = UIKeyboardTypeNumberPad;
    [_phoneNumTV becomeFirstResponder];
    [_BGImgeView addSubview:_phoneNumTV];
}

- (void)verifyAction {
    if (_isInputPhoneNum&&(_phoneNumStr.length == 11)) {
        
        if ([_phoneNumStr isEqualToString:@"17711111113"]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"测试账号无需获取验证码，使用固定验证码6261即可" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
            [alertView show];
            return;
        }
        
        NSLog(@"输入手机号并且格式验证通过，去请求验证码");
        // 60秒倒计时
        __block NSInteger second = 60;
        dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, quene);
        dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
        dispatch_source_set_event_handler(timer, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (second == 0) {
                    self->_getVerifyBtn.userInteractionEnabled = YES;
                    [self->_getVerifyBtn setTitle:[NSString stringWithFormat:@"重新获取"] forState:UIControlStateNormal];
                    second = 60;
                    dispatch_cancel(timer);
                } else {
                    self->_getVerifyBtn.userInteractionEnabled = NO;
                    [self->_getVerifyBtn setTitle:[NSString stringWithFormat:@"%ld秒后重试",(long)second] forState:UIControlStateNormal];
                    second--;
                }
            });
        });
        dispatch_resume(timer);
        _isGetVerify = YES;

        [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:[RequestUrl theMessageCode] loadStr:@"" param:@{@"phone":_phoneNumStr} Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
            if (code == 0) {
                //获取验证码成功
            }else{
                [Tools showMessage:@"获取验证码失败,请稍后重试"];
            }
        } netCompletBlock:^(NSString * _Nonnull netStr) {
            [Tools showMessage:netStr];
        }];
    }else {
        [Tools showMessage:@"请输入正确的手机号"];
    }
}

- (void)inputVerifyAction {
    _verifyTV = [[UITextView alloc]initWithFrame:_verifyBtn.frame];
    [_verifyTV setFont:[UIFont fontWithName:[Tools setFontNamePingFangRegular] size:15*[Tools theSize:@"width"]]];
    [_verifyTV setBackgroundColor:WhiteColor];
    _verifyTV.textColor = [UIColor blackColor];
    _verifyTV.delegate = self;
    _verifyTV.keyboardType = UIKeyboardTypeNumberPad;
    
    if (_isGetVerify) {
        [_verifyTV becomeFirstResponder];
        [_BGImgeView addSubview:_verifyTV];
    }else {
        [_verifyTV becomeFirstResponder];
        [_BGImgeView addSubview:_verifyTV];
        //[Tools showMessage:@"请先获取验证码"];
    }
}



@end
