//
//  PrivacyProtocolViewController.m
//  BlackDoPlanet
//
//  Created by Botao on 2018/9/4.
//  Copyright © 2018年 botaoweb. All rights reserved.
//

#import "PrivacyProtocolViewController.h"

@interface PrivacyProtocolViewController ()<UIWebViewDelegate>

@property (nonatomic, assign) float xWidth;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIWebView *protocolWebView;

@end

@implementation PrivacyProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"用户协议";
    self.view.backgroundColor = [UIColor whiteColor];
    _xWidth = [Tools theSize:@"width"];

    [self initWebView];
    [self initTopView];
    
}

- (void)initTopView
{
    
    _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [Tools getStatusBarHeight]+44*_xWidth)];
    _topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_topView];
    _topView.userInteractionEnabled = YES;

    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(100*_xWidth, [Tools getStatusBarHeight], self.view.frame.size.width-200*_xWidth, 44*_xWidth)];
    title.text = @"用户协议";
    title.textColor = [Tools hexStringToUIColor:@"333333"];
    title.font = [UIFont fontWithName:[Tools setFontNamePingFang] size:20*_xWidth];
    title.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:title];
    
    UIButton *returnBtn = [[UIButton alloc]initWithFrame:CGRectMake(13*_xWidth, [Tools getStatusBarHeight]+10*_xWidth, 25*_xWidth, 25*_xWidth)];
    [returnBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [returnBtn addTarget:self action:@selector(returnBtn) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:returnBtn];
}

- (void)returnBtn
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)initWebView
{
    self.protocolWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, [Tools getStatusBarHeight]+44*_xWidth+1, DeviceWidth, DeviceHeight-([Tools getStatusBarHeight]+44*_xWidth))];
    self.protocolWebView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.protocolWebView];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"090611521759" ofType:@"html"];
    NSString *content = [[NSString alloc] initWithContentsOfFile:path];
    self.protocolWebView.delegate = self;
    [self.protocolWebView loadHTMLString:content baseURL:nil];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [ZK_HUD showUIBlockingIndicatorWithText:@"加载中..."];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [ZK_HUD hideUIBlockingIndicator];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
