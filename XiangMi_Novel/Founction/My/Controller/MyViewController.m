//
//  MyViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 Botao. All rights reserved.
//

#import "MyViewController.h"
#import "MyTestViewController.h"

@interface MyViewController ()

@end

@implementation MyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = WhiteColor;
    
    [self addRightBtn];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sureLogin"] == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"presentLoginVC" object:nil];
        return;
    }
}

- (void)addRightBtn
{
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //rightBtn.backgroundColor = [UIColor redColor];
    rightBtn.frame = CGRectMake(0,0,40*AdaptRatio,25*AdaptRatio);
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14*AdaptRatio];
    [rightBtn setTitle:@"测试" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
}

- (void)rightBtnClick
{
    QuickNavigationPush(MyTestViewController);
}


@end
