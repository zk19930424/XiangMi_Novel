//
//  MyTestViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/16.
//  Copyright © 2019 zk. All rights reserved.
//

#import "MyTestViewController.h"
#import "TestModel.h"

@interface MyTestViewController ()
@property (nonatomic,strong) UIScrollView *typeScrollView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UITextView *textView;
@property (nonatomic,strong) UIView *menuView;
@property (nonatomic,assign) NSInteger isHiddenMenu;
@property (nonatomic,strong) UISlider *slider;
@property (nonatomic,assign) NSInteger fontSize;
@property (nonatomic,strong) UILabel *fontSizeLabel;

@end

@implementation MyTestViewController

- (void)requestForContent
{
    NSDictionary *dict = @{@"string":@"Bearer 528675db-2fde-40a0-a62b-92dcd8897941",
                           @"bk_id":@"10011",
                           @"ch_id":@"220817",
                           @"free":@"0",
                           @"os":@"2",
                           };
    [[HttpManager shareInstance] commonRequestWithRequestWay:@"get" url:NovelUrl loadStr:@"" param:dict Complete:^(NSInteger code, NSDictionary * _Nonnull dictionary) {
        if (code == 0) {
            
        }else{
            [Tools showMessage:dictionary[@"msg"]];
        }
    } netCompletBlock:^(NSString * _Nonnull netStr) {
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseUI:@"测试类"];
    
    //[self requestForContent];
    //return;
    
    self.isHiddenMenu = NO;
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(15*AdaptRatio, AdaptNavigationHeight, DeviceWidth-30*AdaptRatio, DeviceHeight-SafeAreaBottomHeight-AdaptNavigationHeight)];
    self.textView.text = [TestModel returnString1];
    [self.view addSubview:self.textView];
    self.textView.font = [UIFont systemFontOfSize:15*AdaptRatio];
    self.textView.textColor = BlackColor;
    self.textView.textAlignment = NSTextAlignmentLeft;
    self.textView.backgroundColor = WhiteColor;
    self.textView.editable = NO;
    self.textView.selectable = NO;
    self.textView.layoutManager.allowsNonContiguousLayout = NO;
    
    NSInteger fontSize = [[NSUserDefaults standardUserDefaults] integerForKey:@"lastFontSize"];
    if (fontSize == 0) {
        self.fontSize = 15;
    }else{
        self.fontSize = fontSize;
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [self.textView addGestureRecognizer:tapGesture];
    
    
    self.menuView = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight-150*AdaptRatio, DeviceWidth, 150*AdaptRatio)];
    self.menuView.hidden = YES;
    self.menuView.backgroundColor = BGLightGrayColor;
    [self.view addSubview:self.menuView];
    
    UIButton *brightessBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //brightessBtn.backgroundColor = [UIColor redColor];
    brightessBtn.frame = CGRectMake(20*AdaptRatio,10*AdaptRatio,60*AdaptRatio,30*AdaptRatio);
    brightessBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [brightessBtn setTitle:@"亮度" forState:UIControlStateNormal];
    [brightessBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [self.menuView addSubview:brightessBtn];
    
    CGFloat brightess = [[UIScreen mainScreen] brightness];
    
    self.slider = [[UISlider alloc] initWithFrame:CGRectMake(90*AdaptRatio, 10*AdaptRatio, self.menuView.size.width-110*AdaptRatio, 30*AdaptRatio)];
    //self.slider.backgroundColor = YellowColor;
    [self.menuView addSubview:self.slider];
    self.slider.minimumValue = 0.1;
    self.slider.maximumValue = 1;
    [self.slider addTarget:self action:@selector(sliderEvent:) forControlEvents:UIControlEventValueChanged];
    self.slider.value = brightess;
    
    UIButton *lastBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //lastBtn.backgroundColor = [UIColor redColor];
    lastBtn.frame = CGRectMake(20*AdaptRatio,self.menuView.size.height-40*AdaptRatio,60*AdaptRatio,30*AdaptRatio);
    lastBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [lastBtn setTitle:@"上一章" forState:UIControlStateNormal];
    [lastBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [lastBtn addTarget:self action:@selector(lastBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:lastBtn];
    
    UIButton *catalogueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //catalogueBtn.backgroundColor = [UIColor redColor];
    catalogueBtn.frame = CGRectMake(self.menuView.size.width/2-30*AdaptRatio,self.menuView.size.height-40*AdaptRatio,60*AdaptRatio,30*AdaptRatio);
    catalogueBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [catalogueBtn setTitle:@"目录" forState:UIControlStateNormal];
    [catalogueBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [catalogueBtn addTarget:self action:@selector(catalogueBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:catalogueBtn];
    
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //nextBtn.backgroundColor = [UIColor redColor];
    nextBtn.frame = CGRectMake(self.menuView.size.width-80*AdaptRatio,self.menuView.size.height-40*AdaptRatio,60*AdaptRatio,30*AdaptRatio);
    nextBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [nextBtn setTitle:@"下一章" forState:UIControlStateNormal];
    [nextBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:nextBtn];
    
    UIButton *addFontSizeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //addFontSizeBtn.backgroundColor = [UIColor redColor];
    addFontSizeBtn.frame = CGRectMake(self.menuView.size.width/2-75*AdaptRatio,60*AdaptRatio,30*AdaptRatio,30*AdaptRatio);
    addFontSizeBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [addFontSizeBtn setTitle:@"➕" forState:UIControlStateNormal];
    [addFontSizeBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [addFontSizeBtn addTarget:self action:@selector(addFontSizeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:addFontSizeBtn];
    
    self.fontSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.menuView.size.width/2-15*AdaptRatio, 60*AdaptRatio, 30*AdaptRatio, 30*AdaptRatio)];
    [self.menuView addSubview:self.fontSizeLabel];
    self.fontSizeLabel.backgroundColor = WhiteColor;
    self.fontSizeLabel.text = [NSString stringWithFormat:@"%ld",self.fontSize];
    self.fontSizeLabel.textAlignment = NSTextAlignmentCenter;
    self.fontSizeLabel.textColor = BlackColor;
    self.fontSizeLabel.font = [UIFont fontWithName:[Tools setFontNamePingFangRegular] size:16*AdaptRatio];
    self.fontSizeLabel.numberOfLines = 0;
    
    UIButton *reduceFontSizeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //reduceFontSizeBtn.backgroundColor = [UIColor redColor];
    reduceFontSizeBtn.frame = CGRectMake(self.menuView.size.width/2+45*AdaptRatio,60*AdaptRatio,30*AdaptRatio,30*AdaptRatio);
    reduceFontSizeBtn.titleLabel.font = [UIFont systemFontOfSize:16*AdaptRatio];
    [reduceFontSizeBtn setTitle:@"➖" forState:UIControlStateNormal];
    [reduceFontSizeBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    [reduceFontSizeBtn addTarget:self action:@selector(reduceFontSizeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView addSubview:reduceFontSizeBtn];
}
//字号增加
- (void)addFontSizeBtnClick
{
    self.fontSize++;
    self.fontSizeLabel.text = [NSString stringWithFormat:@"%ld",self.fontSize];
    self.textView.font = [UIFont systemFontOfSize:self.fontSize*AdaptRatio];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.fontSize forKey:@"lastFontSize"];
}
//字号减小
- (void)reduceFontSizeBtnClick
{
    self.fontSize--;
    self.fontSizeLabel.text = [NSString stringWithFormat:@"%ld",self.fontSize];
    self.textView.font = [UIFont systemFontOfSize:self.fontSize*AdaptRatio];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.fontSize forKey:@"lastFontSize"];
}
//上一章
- (void)lastBtnClick
{
    self.textView.text = [TestModel returnString2];
    
    [self.textView setContentOffset:CGPointZero animated:NO];
}
//目录
- (void)catalogueBtnClick
{
    
}
//下一章
- (void)nextBtnClick
{
    self.textView.text = [TestModel returnString3];
    
    [self.textView setContentOffset:CGPointZero animated:NO];
}

//显示 菜单栏
- (void)tapGesture
{
    if (self.isHiddenMenu == YES) {
        self.menuView.hidden = YES;
        self.isHiddenMenu = NO;
    }else{
        self.menuView.hidden = NO;
        self.isHiddenMenu = YES;
    }
}

- (void)sliderEvent:(UISlider *)slider
{
    //获取亮度
    //CGFloat brightess = [[UIScreen mainScreen] brightness];
    
    //设置亮度
    [[UIScreen mainScreen] setBrightness:slider.value];
}
















- (void)baseUI:(NSString *)titleStr
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = WhiteColor;
    self.title = titleStr;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self.view addSubview:backBtn];
    //backBtn.backgroundColor = RedColor;
    backBtn.frame = CGRectMake(0,0,25,25);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"heizuan_button_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backBtnItem;
}
- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
