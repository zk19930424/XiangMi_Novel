//
//  TestModel.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/16.
//  Copyright © 2019 zk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestModel : NSObject

+ (NSString *)returnString1;

+ (NSString *)returnString2;

+ (NSString *)returnString3;

@end

NS_ASSUME_NONNULL_END
