//
//  TabBarViewController.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 Botao. All rights reserved.
//

#import "TabBarViewController.h"
#import "PhoneLoginViewController.h"
#import "MyViewController.h"
#import "FindViewController.h"
#import "BookstoreViewController.h"
#import "BookcaseViewController.h"

@interface TabBarViewController ()<UITabBarControllerDelegate>

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentLoginVC) name:@"presentLoginVC" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginOutSkip) name:@"loginOutSkip" object:nil];
    
    BookcaseViewController *mainVC = [[BookcaseViewController alloc]init];
    UINavigationController *mainNav = [[UINavigationController alloc] initWithRootViewController:mainVC];
    BookstoreViewController*shopVC = [[BookstoreViewController alloc]init];
    UINavigationController *shopNav = [[UINavigationController alloc] initWithRootViewController:shopVC];
    FindViewController *creditLifeVC = [[FindViewController alloc]init];
    UINavigationController *creditLifeNav = [[UINavigationController alloc] initWithRootViewController:creditLifeVC];
    MyViewController *myVC = [[MyViewController alloc]init];
    UINavigationController *myNav = [[UINavigationController alloc] initWithRootViewController:myVC];
    
    self.viewControllers = @[mainNav,shopNav,creditLifeNav,myNav];
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBar.translucent = NO;
    self.selectedIndex = 1;
    self.delegate = self;
    UITabBarItem *item0 = self.tabBar.items[0];
    UITabBarItem *item1 = self.tabBar.items[1];
    UITabBarItem *item2 = self.tabBar.items[2];
    UITabBarItem *item3 = self.tabBar.items[3];
    
    
    item0.title = @"书架";
    item0.image = [[UIImage imageNamed:@"book_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    item0.selectedImage = [[UIImage imageNamed:@"bookpressed"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    item1.title = @"书城";
    item1.image = [[UIImage imageNamed:@"bookstore_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    item1.selectedImage = [[UIImage imageNamed:@"bookstore_pressed"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    item2.title = @"发现";
    item2.image = [[UIImage imageNamed:@"find_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    item2.selectedImage = [[UIImage imageNamed:@"find_pressed"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    item3.title = @"我的";
    item3.image = [[UIImage imageNamed:@"my_normal"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    item3.selectedImage = [[UIImage imageNamed:@"my_pressed"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    for (UITabBarItem *item in self.tabBar.items)
    {
        [item setTitleTextAttributes:@{NSForegroundColorAttributeName:ThemeColor, NSFontAttributeName: [UIFont fontWithName:[Tools setFontNamePingFang] size:11]}  forState:UIControlStateSelected];
        [item setTitleTextAttributes:@{NSForegroundColorAttributeName:LightGrayColor, NSFontAttributeName: [UIFont fontWithName:[Tools setFontNamePingFang] size:11]}  forState:UIControlStateNormal];
    }
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
}

- (void)loginOutSkip
{
    self.selectedIndex = 1;
}

- (void)tabBarController:(UITabBarController*)tabBarController didSelectViewController:(UIViewController*)viewController
{
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if (tabBarController.selectedIndex == 0||tabBarController.selectedIndex == 2)
    {
        if ([user boolForKey:@"sureLogin"] == NO) {
            [self presentLoginVC];
            return;
        }
    }else if (tabBarController.selectedIndex == 3){
        if ([user boolForKey:@"sureLogin"] == NO) {
            [self presentLoginVC];
            return;
        }
        
//        [self presentLoginVC];
//        return;
    }
}


//弹出登录界面
- (void)presentLoginVC
{
    self.selectedIndex = 1;
    //清除NSUserDefaults缓存
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user removeObjectForKey:@"isSaveUUID"];
    [user removeObjectForKey:@"sureLogin"];
    [user removeObjectForKey:@"novelUrl"];
    //登录状态为0
    [Dao updateIsLogin:@"0"];
    
    PhoneLoginViewController *login = [[PhoneLoginViewController alloc] init];
    //[self.navigationController pushViewController:login animated:YES];
    [self presentViewController:login animated:YES completion:nil];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentLoginVC" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loginOutSkip" object:nil];
}

@end
