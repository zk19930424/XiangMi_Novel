//
//  NetConnectModel.h
//  LuoHuaiJiaoYu
//
//  Created by sooyooo on 2017/10/19.
//  Copyright © 2017年 sooyooo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetConnectModel : NSObject
+ (instancetype)shareInstance;

- (BOOL) isConnectionAvailable;
@end
