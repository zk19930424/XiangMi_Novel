
//
//  NetConnectModel.m
//  LuoHuaiJiaoYu
//
//  Created by sooyooo on 2017/10/19.
//  Copyright © 2017年 sooyooo. All rights reserved.
//

#import "NetConnectModel.h"
#import "Reachability.h"

@implementation NetConnectModel
+ (instancetype)shareInstance
{
    static NetConnectModel *_shardInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shardInstance = [[NetConnectModel alloc]init];
    });
    return _shardInstance;
}
- (BOOL) isConnectionAvailable{
    
    BOOL isExistenceNetwork = YES;
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:
            isExistenceNetwork = NO;
            //NSLog(@"notReachable");
            break;
        case ReachableViaWiFi:
            isExistenceNetwork = YES;
            //NSLog(@"WIFI");
            break;
        case ReachableViaWWAN:
            isExistenceNetwork = YES;
            //NSLog(@"3G");
            break;
    }
    return isExistenceNetwork;
}
@end
