//
//  Dao.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface Dao : NSObject

+(void)login:(UserModel *)userModel;
+(NSString *)isLogin;
+(UserModel *)findUserInfo;
+(void)updateIsLogin:(NSString *)isLogin;
+(void)updateBlackDrill:(NSString *)blackDrill unBlackDrill:(NSString *)unBlackDrill stamina:(NSString *)stamina;
+(void)updateUserInfoWithNickName:(NSString *)nickname;
+(void)updateUserInfoWithIcon:(NSString *)icon;
+(void)updateUserInfoWithToken:(NSString *)token;

@end

NS_ASSUME_NONNULL_END
