//
//  Dao.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import "Dao.h"
#import "UserInfoCoreDataOperate.h"

@implementation Dao

+(void)login:(UserModel *)userModel{
    NSLog(@"login Dao :%@",userModel.isLogin);
    if ([@"success"isEqualToString:[UserInfoCoreDataOperate updateUserInfo:userModel]]) {
        [Tools showMessage:@"欢迎使用！"];
    }else{
        [Tools showMessage:@"登录即可成为会员！"];
    }
}

+(NSString *)isLogin{
    return [UserInfoCoreDataOperate isLogin];
}

+(UserModel *)findUserInfo{
    
    return [UserInfoCoreDataOperate findUserInfo];
}

+(void)updateIsLogin:(NSString *)isLogin {
    NSString *status = [UserInfoCoreDataOperate updateIsLogin:isLogin];
    if ([@"success"isEqualToString:status]) {
        NSLog(@"update isLogin sucess");
    }else {
        NSLog(@"update isLogin false");
    }
}

+(void)updateBlackDrill:(NSString *)blackDrill unBlackDrill:(NSString *)unBlackDrill stamina:(NSString *)stamina {
    NSString *status = [UserInfoCoreDataOperate updateBlackDrill:blackDrill unBlackDrill:unBlackDrill stamina:stamina];
    if ([@"success"isEqualToString:status]) {
        NSLog(@"update blackDrill sucess");
    }else {
        NSLog(@"update blackDrill false");
    }
}

+(void)updateUserInfoWithNickName:(NSString *)nickname
{
    NSString *status = [UserInfoCoreDataOperate updateUserInfoWithNickName:nickname];
    if ([@"success"isEqualToString:status]) {
        NSLog(@"update blackDrill sucess");
    }else {
        NSLog(@"update blackDrill false");
    }
}

+(void)updateUserInfoWithIcon:(NSString *)icon
{
    NSString *status = [UserInfoCoreDataOperate updateUserInfoWithIcon:icon];
    if ([@"success"isEqualToString:status]) {
        NSLog(@"update blackDrill sucess");
    }else {
        NSLog(@"update blackDrill false");
    }
}

+(void)updateUserInfoWithToken:(NSString *)token
{
    NSString *status = [UserInfoCoreDataOperate updateUserInfoWithIcon:token];
    if ([@"success"isEqualToString:status]) {
        NSLog(@"update blackDrill sucess");
    }else {
        NSLog(@"update blackDrill false");
    }
}


@end
