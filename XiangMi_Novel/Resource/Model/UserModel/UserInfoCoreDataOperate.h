//
//  UserInfoCoreDataOperate.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "UserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserInfoCoreDataOperate : NSObject

+(NSString *)updateUserInfo:(UserModel *)userModel;
+(UserModel *)findUserInfo;

+(NSString *)isLogin;
+(NSString *)updateIsLogin:(NSString *)isLogin;
+(NSString *)updateBlackDrill:(NSString *)blackDrill unBlackDrill:(NSString *)unBlackDrill stamina:(NSString *)stamina;
+(NSString *)updateUserInfoWithNickName:(NSString *)nickname;
+(NSString *)updateUserInfoWithIcon:(NSString *)icon;

@end

NS_ASSUME_NONNULL_END
