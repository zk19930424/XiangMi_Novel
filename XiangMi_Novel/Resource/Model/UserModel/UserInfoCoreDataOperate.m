//
//  UserInfoCoreDataOperate.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import "UserInfoCoreDataOperate.h"

@implementation UserInfoCoreDataOperate

+(NSString *)updateUserInfo:(UserModel *)userModel{
    AppDelegate *myDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = myDelegate.managedObjectContext;
    NSEntityDescription *userInfoEntity = [NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:userInfoEntity];
    NSError *error = nil;
    NSArray *fetchedArr = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedArr.count > 0) {
        NSManagedObjectContext *context = myDelegate.managedObjectContext;
        NSManagedObject *userModelCD = fetchedArr[0];
        [userModelCD setValue:userModel.balance forKey:@"balance"];
        [userModelCD setValue:userModel.firstLogin forKey:@"firstLogin"];
        [userModelCD setValue:userModel.icon forKey:@"icon"];
        [userModelCD setValue:userModel.invitCode forKey:@"invitCode"];
        [userModelCD setValue:userModel.invitCodeFrom forKey:@"invitCodeFrom"];
        [userModelCD setValue:userModel.level forKey:@"level"];
        [userModelCD setValue:userModel.nickName forKey:@"nickName"];
        [userModelCD setValue:userModel.openId forKey:@"openId"];
        [userModelCD setValue:userModel.password forKey:@"password"];
        [userModelCD setValue:userModel.qqid forKey:@"qqid"];
        [userModelCD setValue:userModel.source forKey:@"source"];
        [userModelCD setValue:userModel.telephone forKey:@"telephone"];
        [userModelCD setValue:userModel.userId forKey:@"userId"];
        [userModelCD setValue:userModel.userName forKey:@"userName"];
        [userModelCD setValue:userModel.valid forKey:@"valid"];
        [userModelCD setValue:userModel.vxid forKey:@"vxid"];
        [userModelCD setValue:userModel.wbid forKey:@"wbid"];
        [userModelCD setValue:userModel.token forKey:@"token"];
        
        NSError *updateError;
        [context save:&updateError];
        if (updateError) {
            NSLog(@"%@",updateError);
            return nil;
        }else {
            NSLog(@"userInfo save ok");
            return @"success";
        }
    }else {
        UserModel *userModelCD = (UserModel *)[NSEntityDescription insertNewObjectForEntityForName:@"UserInfo" inManagedObjectContext:context];
        [userModelCD setValue:userModel.balance forKey:@"balance"];
        [userModelCD setValue:userModel.firstLogin forKey:@"firstLogin"];
        [userModelCD setValue:userModel.icon forKey:@"icon"];
        [userModelCD setValue:userModel.invitCode forKey:@"invitCode"];
        [userModelCD setValue:userModel.invitCodeFrom forKey:@"invitCodeFrom"];
        [userModelCD setValue:userModel.level forKey:@"level"];
        [userModelCD setValue:userModel.nickName forKey:@"nickName"];
        [userModelCD setValue:userModel.openId forKey:@"openId"];
        [userModelCD setValue:userModel.password forKey:@"password"];
        [userModelCD setValue:userModel.qqid forKey:@"qqid"];
        [userModelCD setValue:userModel.source forKey:@"source"];
        [userModelCD setValue:userModel.telephone forKey:@"telephone"];
        [userModelCD setValue:userModel.userId forKey:@"userId"];
        [userModelCD setValue:userModel.userName forKey:@"userName"];
        [userModelCD setValue:userModel.valid forKey:@"valid"];
        [userModelCD setValue:userModel.vxid forKey:@"vxid"];
        [userModelCD setValue:userModel.wbid forKey:@"wbid"];
        [userModelCD setValue:userModel.token forKey:@"token"];
        
        NSError *saveError;
        [context save:&saveError];
        if (saveError) {
            NSLog(@"%@",saveError);
            return nil;
        }else {
            NSLog(@"userInfo save ok");
            return @"success";
        }
    }
    return nil;
}

+(UserModel *)findUserInfo{
    AppDelegate *myDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = myDelegate.managedObjectContext;
    NSEntityDescription *userInfo = [NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest * request = [NSFetchRequest new];
    request.entity = userInfo;
    //执行查询
    NSArray *userInfoArray = [context executeFetchRequest:request error:NULL];
    if (userInfoArray.count > 0) {
        UserModel *obj = userInfoArray[0];
        return obj;
    }
    return nil;
}


+(NSString *)isLogin {
    NSString *isLogin;
    AppDelegate *myDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = myDelegate.managedObjectContext;
    NSEntityDescription *userInfo = [NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest * request = [NSFetchRequest new];
    request.entity = userInfo;
    //执行查询
    NSArray *userInfoArray = [context executeFetchRequest:request error:NULL];
    if (userInfoArray.count > 0) {
        UserModel *obj = userInfoArray[0];
        isLogin = obj.isLogin;
    }
    return isLogin;
}

+(NSString *)updateIsLogin:(NSString *)isLogin {
    AppDelegate *myDelegate = (AppDelegate*)(AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = myDelegate.managedObjectContext;
    NSEntityDescription *userInfo = [NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:userInfo];
    NSArray *userInfoArr = [context executeFetchRequest:request error:nil];
    if (userInfoArr.count > 0){
        //更新里面的值
        NSManagedObject *obj = userInfoArr[0];
        [obj setValue:isLogin forKey:@"isLogin"];
    }
    NSError *error;
    [context save:&error];
    if (!error) {
        NSLog(@"isLogin update ok");
        return @"success";
    }else{
        NSLog(@"%@",error);
        return nil;
    }
}

+(NSString *)updateBlackDrill:(NSString *)blackDrill unBlackDrill:(NSString *)unBlackDrill stamina:(NSString *)stamina{
    AppDelegate *myDelegate = (AppDelegate*)(AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = myDelegate.managedObjectContext;
    NSEntityDescription *userInfo = [NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:userInfo];
    NSArray *userInfoArr = [context executeFetchRequest:request error:nil];
    if (userInfoArr.count > 0){
        //更新里面的值
        UserModel *obj = userInfoArr[0];
        [obj setValue:blackDrill forKey:@"blackDrill"];
        [obj setValue:unBlackDrill forKey:@"unBlackDrill"];
        [obj setValue:stamina forKey:@"stamina"];
    }
    NSError *error;
    [context save:&error];
    if (!error) {
        NSLog(@"isLogin update ok");
        return @"success";
    }else{
        NSLog(@"%@",error);
        return nil;
    }
}

+ (NSString *)updateUserInfoWithNickName:(NSString *)nickname
{
    AppDelegate *myDelegate = (AppDelegate*)(AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = myDelegate.managedObjectContext;
    NSEntityDescription *userInfo = [NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:userInfo];
    NSArray *userInfoArr = [context executeFetchRequest:request error:nil];
    if (userInfoArr.count > 0){
        //更新里面的值
        UserModel *obj = userInfoArr[0];
        [obj setValue:nickname forKey:@"nickname"];
    }
    NSError *error;
    [context save:&error];
    if (!error) {
        NSLog(@"isLogin update ok");
        return @"success";
    }else{
        NSLog(@"%@",error);
        return nil;
    }
}

+(NSString *)updateUserInfoWithIcon:(NSString *)icon
{
    AppDelegate *myDelegate = (AppDelegate*)(AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = myDelegate.managedObjectContext;
    NSEntityDescription *userInfo = [NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:userInfo];
    NSArray *userInfoArr = [context executeFetchRequest:request error:nil];
    if (userInfoArr.count > 0){
        //更新里面的值
        UserModel *obj = userInfoArr[0];
        [obj setValue:icon forKey:@"icon"];
    }
    NSError *error;
    [context save:&error];
    if (!error) {
        NSLog(@"isLogin update ok");
        return @"success";
    }else{
        NSLog(@"%@",error);
        return nil;
    }
}

@end
