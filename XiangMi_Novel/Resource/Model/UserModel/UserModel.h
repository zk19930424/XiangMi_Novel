//
//  UserModel.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserModel : NSObject

@property(nonatomic, strong)NSString *balance;
@property(nonatomic, strong)NSString *firstLogin;
@property(nonatomic, strong)NSString *icon;
@property(nonatomic, strong)NSString *invitCode;
@property(nonatomic, strong)NSString *invitCodeFrom;
@property(nonatomic, strong)NSString *level;
@property(nonatomic, strong)NSString *nickName;
@property(nonatomic, strong)NSString *openId;
@property(nonatomic, strong)NSString *password;
@property(nonatomic, strong)NSString *qqid;
@property(nonatomic, strong)NSString *source;
@property(nonatomic, strong)NSString *telephone;
@property(nonatomic, strong)NSString *userId;
@property(nonatomic, strong)NSString *userName;
@property(nonatomic, strong)NSString *valid;
@property(nonatomic, strong)NSString *vxid;
@property(nonatomic, strong)NSString *wbid;
@property(nonatomic, strong)NSString *token;

@property(nonatomic, strong)NSString *isLogin;

- (instancetype)initWithDicForUserInfo:(NSDictionary *)userInfo;

@end



NS_ASSUME_NONNULL_END


