//
//  UserModel.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

- (instancetype)initWithDicForUserInfo:(NSDictionary *)userInfo
{
    self.balance = [userInfo objectForKey:@"balance"]==nil?@"":[userInfo objectForKey:@"balance"];
    self.firstLogin = [userInfo objectForKey:@"firstLogin"]==nil?@"":[userInfo objectForKey:@"firstLogin"];
    self.icon = [userInfo objectForKey:@"icon"]==nil?@"":[userInfo objectForKey:@"icon"];
    self.invitCode = [userInfo objectForKey:@"invitCode"]==nil?@"":[userInfo objectForKey:@"invitCode"];
    self.invitCodeFrom = [userInfo objectForKey:@"invitCodeFrom"]==nil?@"":[userInfo objectForKey:@"invitCodeFrom"];
    self.level = [userInfo objectForKey:@"level"]==nil?@"":[userInfo objectForKey:@"level"];
    self.nickName = [userInfo objectForKey:@"nickName"]==nil?@"":[userInfo objectForKey:@"nickName"];
    self.openId = [userInfo objectForKey:@"openId"]==nil?@"":[userInfo objectForKey:@"openId"];
    self.password = [userInfo objectForKey:@"password"]==nil?@"":[userInfo objectForKey:@"password"];
    self.qqid = [userInfo objectForKey:@"qqid"]==nil?@"":[userInfo objectForKey:@"qqid"];
    self.source = [userInfo objectForKey:@"source"]==nil?@"":[userInfo objectForKey:@"source"];
    self.telephone = [userInfo objectForKey:@"telephone"]==nil?@"":[userInfo objectForKey:@"telephone"];
    self.userId = [userInfo objectForKey:@"userId"]==nil?@"":[userInfo objectForKey:@"userId"];
    self.userName = [userInfo objectForKey:@"userName"]==nil?@"":[userInfo objectForKey:@"userName"];
    self.valid = [userInfo objectForKey:@"valid"]==nil?@"":[userInfo objectForKey:@"valid"];
    self.vxid = [userInfo objectForKey:@"vxid"]==nil?@"":[userInfo objectForKey:@"vxid"];
    self.wbid = [userInfo objectForKey:@"wbid"]==nil?@"":[userInfo objectForKey:@"wbid"];
    self.token = [userInfo objectForKey:@"token"]==nil?@"":[userInfo objectForKey:@"token"];
    
    self.isLogin = [userInfo objectForKey:@"isLogin"]==nil?@"":[userInfo objectForKey:@"isLogin"];
    return self;
}

@end

