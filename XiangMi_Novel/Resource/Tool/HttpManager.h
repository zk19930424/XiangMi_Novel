//
//  HttpManager.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^completeWithDicBlock)(NSInteger code, NSDictionary *dictionary);
typedef void (^completeWithArrayBlock)(NSInteger code, NSDictionary *array);

@interface HttpManager : NSObject

+ (HttpManager *)shareInstance;

//隐藏接口
- (void)getFlagParam:(NSDictionary *)paramDic Complete:(completeWithDicBlock)dicBlock;

//登录接口
- (void)loginRequestWithUrl:(NSString *)url param:(NSDictionary *)paramDic token:(NSString *)token Complete:(completeWithDicBlock)dicBlock;
//绑定接口
- (void)bindingRequestWithUrl:(NSString *)url param:(NSDictionary *)paramDic Complete:(completeWithDicBlock)dicBlock;
//上传图片
- (void)requestUploadImageWithUrl:(NSString *)url param:(NSDictionary *)param file:(id)file token:(NSString *)token Complete:(completeWithDicBlock)dicBlock;

//通用整合接口，参数：requestWay，url，dict
- (void)commonRequestWithRequestWay:(NSString *)requestWay url:(NSString *)url loadStr:(NSString *)loadStr param:(NSDictionary *)paramDic Complete:(completeWithDicBlock)dicBlock netCompletBlock:(void(^)(NSString *netStr))netBlock;

@end

NS_ASSUME_NONNULL_END
