//
//  RequestUrl.h
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestUrl : NSObject

//登录
+ (NSString *)TheLogin_post;
//绑定
+ (NSString *)TheBinding_post;
// 发送验证码
+ (NSString *)theMessageCode;
//是否隐藏
+ (NSString *)iosFlag;
//邀请码提交
+ (NSString*)theInvitCodeUrlHost;

//我的书架
+ (NSString *)theMyBookcase_get;
//加入书架
+ (NSString *)theBookaddToBookcase_post;
//移出书架
+ (NSString *)theDeleteFromMyBookcase_post;



#pragma 书城
//书城 轮播图
+ (NSString *)theBookStoreBanner_get;
//重磅推荐
+ (NSString *)theBookStoreRecommennd_get;
//一周热读
+ (NSString *)theBookStoreWeekHotRead_get;
//畅销小说
+ (NSString *)theBookStoreHotSelling_get;
//书籍详情
+ (NSString *)theBookDetail_get;


//章节目录
+ (NSString *)theBookChapterCatalogue_post;
//书籍内容
+ (NSString *)theBookReadContent_post;
//上一章
+ (NSString *)theBooklastChapter_post;
//下一章
+ (NSString *)theBookNextChapter_post;



@end

NS_ASSUME_NONNULL_END
