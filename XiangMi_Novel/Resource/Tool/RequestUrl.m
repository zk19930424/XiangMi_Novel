//
//  RequestUrl.m
//  XiangMi_Novel
//
//  Created by Botao on 2019/1/14.
//  Copyright © 2019 zk. All rights reserved.
//

#import "RequestUrl.h"

@implementation RequestUrl


//登录
+ (NSString *)TheLogin_post
{
    return BaseUrl_Append(@"/user/login");
}
//绑定
+ (NSString *)TheBinding_post
{
    return BaseUrl_Append(@"/user/binding");
}
// 发送验证码
+ (NSString *)theMessageCode;
{
    return BaseUrl_Append(@"/api/ali/sendcode");
}
//是否隐藏
+(NSString *)iosFlag
{
    return BaseUrl_Append(@"/user/info/flag");
}
//邀请码提交
+(NSString*)theInvitCodeUrlHost
{
    return BaseUrl_Append(@"/user/invitcode");
}


//我的书架
+ (NSString *)theMyBookcase_get
{
    return BaseUrl_Append(@"/list-book-shelf");
}
//加入书架
+ (NSString *)theBookaddToBookcase_post
{
    return BaseUrl_Append(@"/add-book-shelf");
}
//移出书架
+ (NSString *)theDeleteFromMyBookcase_post
{
    return BaseUrl_Append(@"/remove-book-shelf");
}


#pragma 书城
//书城 轮播图
+ (NSString *)theBookStoreBanner_get
{
    return BaseUrl_Append(@"/carousel-pic");
}
//重磅推荐
+ (NSString *)theBookStoreRecommennd_get
{
    return BaseUrl_Append(@"/book/heavy-recommend");
}
//一周热读
+ (NSString *)theBookStoreWeekHotRead_get
{
    return BaseUrl_Append(@"/book/week-recommend");
}
//畅销小说
+ (NSString *)theBookStoreHotSelling_get
{
    return BaseUrl_Append(@"/book/featured-best-seller");
}
//书籍详情
+ (NSString *)theBookDetail_get
{
    return BaseUrl_Append(@"/book/book-info");
}



//章节目录
+ (NSString *)theBookChapterCatalogue_post
{
    return BaseUrl_Append(@"");
}
//书籍内容
+ (NSString *)theBookReadContent_post
{
    return BaseUrl_Append(@"");
}
//上一章
+ (NSString *)theBooklastChapter_post
{
    return BaseUrl_Append(@"");
}
//下一章
+ (NSString *)theBookNextChapter_post
{
    return BaseUrl_Append(@"");
}

@end
