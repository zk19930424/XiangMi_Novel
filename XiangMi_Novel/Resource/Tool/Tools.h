//
//  Tools.h
//  blocklover
//
//  Created by 冯业宁 on 2018/7/11.
//  Copyright © 2018年 Allen.feng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SDUpdate.h"
//456489468156145
@interface Tools : NSObject

#pragma mark 获取时间差根据开始时间戳
+(NSString*)getTimeDiffWithStartTime:(long)StartTime;

#pragma mark 获取设备尺寸
+(CGSize)getDiviceSize;

#pragma mark 获取动态行高
+(float)getDynamicHeightWithWidth:(float)width fontSize:(float)fontSize Text:(NSString *)text numOfLins:(int)num spacing:(int)spacing;

#pragma mark 获取富文本动态行高
+(CGFloat)getSpaceLabelHeight:(NSString *)str withAttrDict:(NSMutableDictionary *)dict withWidth:(CGFloat)width linesNum:(int)linesNum;

#pragma mark 获取label的size
+(CGSize)getDynamicSizeWithText:(NSString *)text font:(UIFont *)font;

#pragma mark 获取导航栏高度
+(float)getStatusBarHeight;
    
#pragma mark 设置苹方字体
+(NSString*)setFontNamePingFang;

#pragma mark 设置苹方字体Bold
+(NSString*)setFontNamePingFangBold;

#pragma mark 设置苹方字体Regular
+(NSString*)setFontNamePingFangRegular;

#pragma mark 获取当前时间戳
+(NSString *)getDateTime;

#pragma mark 16进制颜色转UIColor
+(UIColor *) hexStringToUIColor: (NSString *) hexString;

#pragma mark 提示信息弹出自动消失
+(void)showMessage:(NSString *)message;

#pragma mark 获取当前屏幕中present出来的viewcontroller
+(UIViewController *)getPresentedViewController;

#pragma mark 屏幕适配
+(float)theSize:(NSString*)sizeName;

#pragma mark 获取IP
+(NSDictionary *)deviceWANIPAdress;

#pragma mark 生成清晰二维码
+(UIImage *)creatNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat)size;

#pragma mark 字典判空字段
+ (NSString *)judgeNullDict:(NSDictionary *)dict string:(NSString *)string;
//判断版本更新
+ (void)versionTip:(UIViewController *)vc;
//时间戳转字符串
+ (NSString *)formateDate:(NSString *)string;
//获取当前时间戳
+ (NSString *)currentTimeStr;
//手机号加密
+ (NSString *)secretPhoneNumber:(NSString *)str;

// 判断用户是否允许接收通知
+ (BOOL)isUserNotificationEnable;

//如果用户关闭了接收通知功能，该方法可以跳转到APP设置页面进行修改
+ (void)goToAppSystemSetting;

//自适应宽度
/*
 p1:文本
 p2:label对象，目的是调用属性使字体适应宽度
 p3:字体大小
 */
+ (CGSize)getFitWidthText:(NSString *)text label:(UILabel *)label fontSize:(CGFloat)fontSize;
//自适应高度
/*
 p1:label文本
 p2:固定宽度
 p3:字体大小
 */
+ (CGSize)getFitHeightText:(NSString *)text labelWidth:(CGFloat)labelWidth fontSize:(CGFloat)fontSize;


+ (NSString *)setSTSongti_Sc_Bold;

//可变属性字符串
+ (NSMutableAttributedString *)backStr:(NSDictionary *)dict1 str2:(NSDictionary *)dict2;
//字符串截取
+ (NSString *)separateString:(NSString *)string length:(NSInteger)length location:(NSInteger)location;

@end

